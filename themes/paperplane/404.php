<?php 
	get_header(); 
	$theme_options = _WSH()->option();
?>
<div style="background-image: url(<?php echo esc_url(sh_set($theme_options , 'title_bg_404_image')); ?>);" id="home-blog-section">
	<div id="home-section-overlayer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="blog-title">
					<?php echo esc_html(sh_set($theme_options , 'page_404_title')) ; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
		
			<h1 style="font-size:180px; margin-top:1%;">
				<?php echo esc_html(sh_set($theme_options , 'page_404_heading')) ; ?>
			</h1>
		
			<h2 style="font-size:50px; margin-top:1%;">
				<?php echo esc_html(sh_set($theme_options , 'page_404_subheading')) ; ?>
			</h2>
			
			<h5 style="font-size:30px; margin-top:1%; margin-bottom:5%;">
				<a href="<?php echo esc_url(home_url());?>">
					<i class="fa fa-reply-all"></i> <?php esc_html_e(" Back To Home" , SH_NAME);?>
				</a>
			</h5>
			
		</div>
	</div>
</div>
	
<?php get_footer(); ?>