<?php 
	get_header(); 
	$theme_options = _WSH()->option();
	global $wp_query ;
	$main_class = (sh_set($theme_options , 'author_page_sidebar') == 'default-sidebar') ? 'col-md-9' : 'col-md-12' ;
?>

<div style="background-image: url(<?php echo esc_url(sh_set($theme_options , 'author_title_bg_image')); ?>);" id="home-blog-section">
	<div id="home-section-overlayer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="blog-title">
					<?php the_author(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="blog-breadcrumb-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php
					if(sh_set($theme_options , 'show_author_breadcrum')):
						echo get_the_breadcrumb();
					endif;
				?>
			</div>
		</div>
	</div>
</div>

<div id="blog-content-section">
	<div class="container">
		<div class="row">
			<div class="<?php echo esc_attr($main_class) ; ?>">

				<?php
					if(have_posts()):  while(have_posts()): the_post(); 
					global $post ;
				?>
				<div <?php post_class('post-container'); ?>>
					<?php if(has_post_thumbnail()): ?>
					<div class="post-image">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail('845x530', array( 'class' => 'img-responsive' )); ?>
						</a>
					</div>
					<?php endif; ?>
					
					<div class="post-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_title(); ?>
						</a>
					</div>

					<div class="post-meta">
						<span><?php echo get_the_date('j'); ?></span>
						<span><?php echo get_the_date('M'); ?></span>
					</div>

					<div class="post-content">
						<?php the_excerpt(); ?>
					</div>

					<div class="post-button">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-meduim btn-nesto-o">
							<?php esc_html_e("Read More", SH_NAME); ?>
						</a>
					</div>

				</div>
				<?php
					endwhile ; endif ;
					wp_reset_query();
				?>
				
				<ul class="pager">
					<li class="previous"><?php previous_posts_link( '&larr; '.__("Older" , SH_NAME) ); ?></li>
					<li class="next"><?php next_posts_link( __("Newer" , SH_NAME).' &rarr;' ); ?></li>
				</ul>

			</div>

			<?php if(sh_set($theme_options , 'author_page_sidebar') == 'default-sidebar'): ?>
				<div class="col-md-3">
					<?php dynamic_sidebar(sh_set($theme_options , 'author_page_sidebar')); ?> 
				</div>
			<?php endif; ?>
			
		</div>
	</div>

</div>
<!-- /blog-content-section -->
<script>
	jQuery(document).ready(function($) {
		 jQuery(this).find('.post-image img').each(function () {
			jQuery(this).addClass('grayscale');
		});
	});
</script>
<?php get_footer(); ?>