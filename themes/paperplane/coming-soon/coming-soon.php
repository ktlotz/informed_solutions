<!DOCTYPE html>
	<?php
		$theme_options = _WSH()->option();
		$layout = sh_set($theme_options , 'theme_layouts');
		$social_icons = sh_set(sh_set($theme_options , 'social_icons') , 'social_icons');
	?>
	<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
	<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
	<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php wp_title( '-', true, 'right' ); ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="GOOGLEBOT" content="index follow" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		
		<?php if ( sh_set( $theme_options, 'site_favicon' ) ) : ?>
			<link rel="icon" type="image/png" href="<?php echo esc_url(sh_set( $theme_options, 'site_favicon' )); ?>">
		<?php endif; ?>
		
		<!-- =========================================
		CSS
		========================================== -->
		<?php wp_head(); ?>
	</head><!-- /head -->
	
	<!-- =========================================
	body
	========================================== -->
	<body <?php body_class($layout.' coming-soon'); ?>>
		<!--[if lt IE 7]>
			<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
		<![endif]-->
		<!-- Loader -->
		<?php if(sh_set($theme_options , 'cs_preloader')): ?>
		<div id="loader">
			<div id="loader-container">
				<img src="<?php echo esc_url(sh_set($theme_options , 'cs_preloader_image')); ?>" alt="<?php esc_attr_e("Loading..." , SH_NAME) ; ?>" />
			</div>
		</div>
		<?php endif; ?>
        <!-- home-section -->
        <?php if(sh_set($theme_options , 'coming_soon_slider')):
			if(sh_set($theme_options , 'slides_no') > 0):
				if(sh_set($theme_options , 'cs_slider')):
		?>
				<div <?php if(sh_set($theme_options , 'theme_layouts')=='parallax-image'):?>style="background-image: url(<?php echo esc_url(sh_set($theme_options , 'parallax_image_bg')); ?>);"<?php endif; ?> id="home-section">
					<div id="home-section-overlayer"></div>
					<div id="owl-home-wrapper">
						<div class="owl-home">
							<?php 
							  $args = array('post_type' => 'sh_slider' , 'posts_per_page' => sh_set($theme_options , 'slides_no') ) ; 
							  query_posts($args);
							  if(have_posts()):  while(have_posts()): the_post(); 
							  global $post ; 
							  $meta = get_post_meta(get_the_ID() , 'slider_meta' , true);
							?>
							
							<?php if(sh_set($meta , 'slide_layout')=='right_image'): ?>
							<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
								<div class="slide-content">
									<div class="col-md-5">
										<div class="slide-title">
											<?php echo esc_html(sh_set($meta , 'slide_text')); ?>
										</div>
										<div class="slide-desc">
											<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
										</div>
										<div class="slide-divider"></div>
										<div class="slide-button">
											<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
												<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
											</a>
										</div>
									</div>
									<div class="col-md-7">
										<div class="feature-left-image">
											<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
										</div>
									</div>
								</div>
							</div>
							<?php endif;?>
							
							<?php if(sh_set($meta , 'slide_layout')=='left_image'): ?>
							<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
								<div class="slide-content">
									<div class="col-md-7">
										<div class="feature-left-image">
											<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
										</div>
									</div>
									<div class="col-md-5">
										<div class="slide-title">
											<?php echo esc_html(sh_set($meta , 'slide_text')); ?>
										</div>
										<div class="slide-desc">
											<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
										</div>
										<div class="slide-divider"></div>
										<div class="slide-button">
											<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
												<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php endif;?>
							
							<?php if(sh_set($meta , 'slide_layout')=='top_image'): ?>
							<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
								<div class="slide-content">
									<div class="slide-logo">
										<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
									</div>
									<div class="slide-title">
									   <?php echo esc_html(sh_set($meta , 'slide_text')); ?>
									</div>
									<div class="slide-desc">
										<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
									</div>
									<div class="slide-divider"></div>
									<div class="slide-button">
										<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
											<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
										</a>
									</div>
								</div>
							</div>
							<?php endif;?>
							
							<?php if(sh_set($meta , 'slide_layout')=='bottom_image'): ?>
							<div class="slide-item no-bottom-padding" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
								<div class="slide-content">
									<div class="slide-title">
										<?php echo esc_html(sh_set($meta , 'slide_text')); ?>
									</div>
									<div class="slide-desc">
										<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
									</div>
									<div class="slide-divider"></div>
									<div class="slide-button">
										<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
											<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
										</a>
									</div>
									<div class="feature-bottom-image">
										<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
									</div>
								</div>
							</div>
							<?php endif;?>
							
							<?php 
							  endwhile ; endif ;
							  wp_reset_query();
							?>
						</div>
						
						<?php if( sh_set($theme_options , 'slides_no') > 1 ) :?>
						<div class="owl-navigation">
							<a class="home-prev"><i class='fa fa-angle-left'></i></a>
							<a class="home-next"><i class='fa fa-angle-right'></i></a>
						</div>
						<?php endif; ?>
						
					</div>
				</div>
				<?php if(sh_set($theme_options , 'theme_layouts') == 'video-background'): ?>
				<?php wp_enqueue_script( 'okvideo' ); ?>
				<script>
					jQuery(document).ready(function($) {
						if (jQuery('body').hasClass('video-background')) {
								/* ==========================================================================
								okvideo
								========================================================================== */
								jQuery(function () {
									jQuery('.video-background #home-section').okvideo({
										hd: true,
										volume: 0,
										loop: true,
										adproof: true,
										autoplay: true,
										annotations: false,
										source: '<?php echo esc_js(sh_set($theme_options , 'video_background_link')); ?>'
									});
								});
							}
					});
				</script>
				<?php endif; ?>
		<?php
				endif;
			endif;
			  endif;
			  
			if(sh_set($theme_options , 'coming_soon_alert')):
		?>
		<!-- Alert Now -->
		<div id="alert-section">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="short-section-title">
						<?php echo esc_html(sh_set($theme_options , 'coming_alert_tagline'));?>
					</div>
				</div>
				<div class="col-md-3">
					<a href="<?php echo esc_url(sh_set($theme_options , 'coming_alert_button_link'));?>" title="<?php echo esc_attr(sh_set($theme_options , 'coming_alert_button_text')) ; ?>" class="btn btn-meduim btn-nesto">
						<?php echo esc_html(sh_set($theme_options , 'coming_alert_button_text')) ; ?>
					</a>
				</div>
			</div>
		</div>
		</div>
		<?php
			endif;
			
			if(sh_set($theme_options , 'coming_soon_time')):
				wp_enqueue_script( 'lwtCountdown' );
				$time_to_open = explode("-",sh_set($theme_options , 'coming_time_date'));
				$date_to_open = sh_set( $time_to_open , 0 ) ;
				$month_to_open = sh_set( $time_to_open , 1 ) ;
				$year_to_open = sh_set( $time_to_open , 2 ) ;
				
		?>
		<!-- countdown-section -->
		<div id="countdown-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<i class="fa fa-clock-o fa-spin"></i> 
						<?php echo esc_html(sh_set($theme_options , 'coming_time_title')) ; ?>
					</div>
				</div>
				<div class="col-md-12">
					<div id="countdown_dashboard">
						<div class="col-md-3 col-sm-3 col-xs-6">
							<div class="dash days_dash">
								<div class="digit">0</div>
								<div class="digit">0</div>
								<div class="digit">0</div>
								<span class="dash_title"><?php esc_html_e("Days" , SH_NAME); ?></span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6">
							<div class="dash hours_dash">
								<div class="digit">0</div>
								<div class="digit">0</div>
								<span class="dash_title"><?php esc_html_e("Hours" , SH_NAME) ; ?></span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6">
							<div class="dash minutes_dash">
								<div class="digit">0</div>
								<div class="digit">0</div>
								<span class="dash_title"><?php esc_html_e("Minutes" , SH_NAME); ?></span>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6">
							<div class="dash seconds_dash">
								<div class="digit">0</div>
								<div class="digit">0</div>
								<span class="dash_title"><?php esc_html_e("Seconds" , SH_NAME) ; ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<script>
		jQuery(document).ready(function($) {
			if (jQuery('body').hasClass('coming-soon')) {
				jQuery('#countdown_dashboard').countDown({
					targetDate: {
						'day': <?php echo esc_js($date_to_open); ?>,
						'month': <?php echo esc_js($month_to_open); ?>,
						'year': <?php echo esc_js($year_to_open); ?>,
						'hour': 0,
						'min': 0,
						'sec': 0
					},
					omitWeeks: true
				});
			}
		});
		</script>
		<?php
			endif;
			
			if(sh_set($theme_options , 'coming_soon_newsletter')):
			$bg_img_src = esc_url(sh_set($theme_options , 'coming_newsletter_bg_image'));
		?>
		<!-- subscribe-section -->
		<div style="background-image: url(<?php echo esc_url($bg_img_src); ?>);" id="subscribe-section">
		<div id="subscribe-section-overlayer">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="short-section-title">
							<?php echo esc_html(sh_set($theme_options , 'coming_newsletter_title'));?>
						</div>
					</div>
					<div class="col-md-12">
						<form id="newsletter-form" role="form" action="<?php echo esc_url("http://feedburner.google.com/fb/a/mailverify"); ?>" accept-charset="utf-8" method="post">
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="<?php esc_attr_e("Enter your e-mail address" , SH_NAME) ; ?>" name="email" value="">
									</div>
								</div>
								<input type="hidden" id="uri" name="uri" value="<?php echo esc_attr(sh_set($theme_options , 'coming_newsletter_feed_id'));?>">
								<input type="hidden" value="en_US" name="loc">
								<div class="col-md-3">
									<div class="form-group">
										<button type="submit" class="btn btn-meduim btn-nesto">
											<?php echo esc_html(sh_set($theme_options , 'coming_newsletter_button'));?>
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		<?php
			endif;
			
			if(sh_set($theme_options , 'coming_soon_contact')):
		?>
		<!-- contact-section -->
		<div id="contact-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">
					<div class="section-title">
						<?php echo esc_html(sh_set($theme_options , 'coming_contact_title')) ; ?>
					</div>
				</div>
				<div class="col-md-12 wow pulse" data-wow-duration="1.5s">
					<div class="row">
						<form class="contact-form" id="contactform" method="post" action="<?php echo admin_url('admin-ajax.php?action=_sh_ajax_callback&amp;subaction=sh_contact_form_submit'); ?>">
							<div class="msgs"></div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="<?php esc_attr_e("Name" , SH_NAME); ?>" name="contact_name" id="name">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="<?php esc_attr_e("Email" , SH_NAME); ?>" name="contact_email" id="email">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="<?php esc_attr_e("Subject" , SH_NAME); ?>" name="contact_subject" id="subject">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea rows="10" class="form-control" placeholder="<?php esc_attr_e("Message" , SH_NAME); ?>" name="contact_message" id="message"></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<button type="submit" id="contact_form_submit" class="btn btn-lg btn-nesto-o" name="contact_submit">
								<?php esc_html_e("Send Now" , SH_NAME); ?>
							</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		<script>
		jQuery(document).ready(function($) {
			$('#contactform').live('submit', function(e){
				e.preventDefault();
				var thisform = this;
				var fields = $(this).serialize();
				var url= $(this).attr('action');
				$.ajax({
					url: url,
					type: 'POST',
					data: fields,
					success:function(res){
						//salert(res);
						$('.msgs', thisform).html(res);
					}
				});
			});
			
			
		});
		</script>
		<?php
			endif;
			
			if(sh_set($theme_options , 'show_map')):
		?>
        <!-- map-section -->
        <div id="map-section">
            <div id="map"></div>
		</div>
		<?php wp_enqueue_script('googleapis' , 'http://maps.google.com/maps/api/js?sensor=true'); ?>
		<script>
		jQuery(document).ready(function($) {
		/* ==========================================================================
			Map
		========================================================================== */
			function init() {
		
				var myLatlng, mapOptions, mapElement, map, markerimage, marker, styleSs;
		
				myLatlng = new google.maps.LatLng(<?php echo esc_js(sh_set($theme_options , 'map_latitude')); ?>, <?php echo esc_js(sh_set($theme_options , 'map_longitude')); ?>);
				mapOptions = {
					zoom: 16,
					panControl: false,
					scrollwheel: false,
					mapTypeControl: true,
					center: myLatlng,
					styles: [
						{
							"featureType": "landscape",
							"stylers": [
								{"saturation": -100},
								{"lightness": 65},
								{"visibility": "on"}
							]
						}, {
							"featureType": "poi",
							"stylers": [
								{"saturation": -100},
								{"lightness": 51},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "road.highway",
							"stylers": [
								{"saturation": -100},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "road.arterial",
							"stylers": [
								{"saturation": -100},
								{"lightness": 30},
								{"visibility": "on"}
							]
						}, {
							"featureType": "road.local",
							"stylers": [
								{"saturation": -100},
								{"lightness": 40},
								{"visibility": "on"}
							]
						}, {
							"featureType": "transit",
							"stylers": [
								{"saturation": -100},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "administrative.province",
							"stylers": [
								{"visibility": "off"}
							]
						}, {
							"featureType": "water",
							"elementType": "labels",
							"stylers": [
								{"visibility": "on"},
								{"lightness": -25},
								{"saturation": -100}
							]
						}, {
							"featureType": "water",
							"elementType": "geometry",
							"stylers": [
								{"hue": "#ffff00"},
								{"lightness": -25},
								{"saturation": -97}
							]
						}
					]
				};
				mapElement = document.getElementById('map');
				map = new google.maps.Map(mapElement, mapOptions);
				markerimage = '<?php echo esc_url(get_template_directory_uri('template_url'));?>/images/marker.png';
				marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					icon: markerimage
				});
			}
		
			testconnection = navigator.onLine;
			if (testconnection) {
				google.maps.event.addDomListener(window, 'load', init);
			}
		});
		</script>
		<!-- /map-section -->
		<?php endif; ?>
        <!-- footer-section -->
        <div id="footer-section">
            <div class="container">
                <div class="row">
					<?php if(sh_set($theme_options , 'copyright_text')): ?>
                    <div class="col-lg-6 col-md-12">
                        <div class="copyright">
                            <?php echo esc_html(sh_set($theme_options , 'copyright_text')); ?>
                        </div>
                    </div>
					<?php
						endif;
						if(count($social_icons > 1)):
					?>
                    <div class="col-lg-6 col-md-12">
                        <div class="social-icons">
							<?php
								foreach((array)$social_icons as $social_icon):
								if(sh_set($social_icon , 'tocopy')) break;
							?>
                            <a href="<?php echo esc_url(sh_set($social_icon , 'site_contact_icon_link')); ?>" title="<?php echo esc_html(sh_set($social_icon , 'site_contact_icon_title')); ?>">
                                <i class="fa <?php echo esc_attr(sh_set($social_icon , 'cs_social_media_icon')); ?>"></i>
                            </a>
							<?php endforeach; ?>
                        </div>
                    </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
        <!--[if lt IE 9]>
            <script src="<?php echo esc_url(get_template_directory_uri('template_url'));?>/js/plugins/html5shiv.js"></script>
            <script src="<?php echo esc_url(get_template_directory_uri('template_url'));?>/js/plugins/selectivizr.js"></script>
        <![endif]-->
		<?php wp_footer(); ?>
		
    </body>
</html>