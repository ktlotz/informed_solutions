<?php
	$theme_options = _WSH()->option(); 
	$social_icons = sh_set(sh_set($theme_options , 'social_icons') , 'social_icons');
?>
		<?php if(sh_set($theme_options , 'show_map')): ?>
        <!-- =========================================
        Map Section
        ========================================== -->
        <!-- map-section -->
        <div id="map-section">

			<!-- map -->
            <div id="map"></div>

		</div>
		<?php wp_enqueue_script('googleapis' , 'http://maps.google.com/maps/api/js?sensor=true'); ?>
		<script>
		jQuery(document).ready(function($) {
		/* ==========================================================================
			Map
		========================================================================== */
			function init() {
		
				var myLatlng, mapOptions, mapElement, map, markerimage, marker, styleSs;
		
				myLatlng = new google.maps.LatLng(<?php echo esc_js(sh_set($theme_options , 'map_latitude')); ?>, <?php echo esc_js(sh_set($theme_options , 'map_longitude')); ?>);
				mapOptions = {
					zoom: 16,
					panControl: false,
					scrollwheel: false,
					mapTypeControl: true,
					center: myLatlng,
					styles: [
						{
							"featureType": "landscape",
							"stylers": [
								{"saturation": -100},
								{"lightness": 65},
								{"visibility": "on"}
							]
						}, {
							"featureType": "poi",
							"stylers": [
								{"saturation": -100},
								{"lightness": 51},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "road.highway",
							"stylers": [
								{"saturation": -100},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "road.arterial",
							"stylers": [
								{"saturation": -100},
								{"lightness": 30},
								{"visibility": "on"}
							]
						}, {
							"featureType": "road.local",
							"stylers": [
								{"saturation": -100},
								{"lightness": 40},
								{"visibility": "on"}
							]
						}, {
							"featureType": "transit",
							"stylers": [
								{"saturation": -100},
								{"visibility": "simplified"}
							]
						}, {
							"featureType": "administrative.province",
							"stylers": [
								{"visibility": "off"}
							]
						}, {
							"featureType": "water",
							"elementType": "labels",
							"stylers": [
								{"visibility": "on"},
								{"lightness": -25},
								{"saturation": -100}
							]
						}, {
							"featureType": "water",
							"elementType": "geometry",
							"stylers": [
								{"hue": "#ffff00"},
								{"lightness": -25},
								{"saturation": -97}
							]
						}
					]
				};
				mapElement = document.getElementById('map');
				map = new google.maps.Map(mapElement, mapOptions);
				markerimage = '<?php echo esc_url(get_template_directory_uri('template_url'));?>/images/marker.png';
				marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					icon: markerimage
				});
			}
		
			testconnection = navigator.onLine;
			if (testconnection) {
				google.maps.event.addDomListener(window, 'load', init);
			}
		});
		</script>
		<!-- /map-section -->
		<?php endif; ?>
        <!-- =========================================
        Footer Section
        ========================================== -->
        <!-- footer-section -->
        <div id="footer-section">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">

					<?php if(sh_set($theme_options , 'copyright_text')): ?>
                    <!-- col-lg-6 col-md-12 -->
                    <div class="col-lg-6 col-md-12">
                        <div class="copyright">
                            <?php echo esc_html(sh_set($theme_options , 'copyright_text')); ?>
                        </div>
                    </div>
					<!-- /col-lg-6 col-md-12 -->
					<?php
						endif;
						if(count($social_icons > 1)):
					?>
                    <!-- col-lg-6 col-md-12 -->
                    <div class="col-lg-6 col-md-12">

                        <!-- social-icons -->
                        <div class="social-icons">
							<?php
								foreach((array)$social_icons as $social_icon):
								if(sh_set($social_icon , 'tocopy')) break;
							?>
                            <a href="<?php echo esc_url(sh_set($social_icon , 'site_contact_icon_link')); ?>" title="<?php echo esc_attr(sh_set($social_icon , 'site_contact_icon_title')) ; ?>">
                                <i class="fa <?php echo esc_attr(sh_set($social_icon , 'cs_social_media_icon')); ?>"></i>
                            </a>
							<?php endforeach; ?>
                        </div><!-- /social-icons -->

                    </div>
					<!-- /col-lg-6 col-md-12 -->
					<?php endif; ?>

                </div><!-- /row -->
            </div><!-- /container -->

        </div><!-- /footer-section -->

        <!-- =========================================
        java script
        ========================================== -->

        <!--[if lt IE 9]>
            <script src="<?php echo esc_url(get_template_directory_uri('template_url'));?>/js/plugins/html5shiv.js"></script>
            <script src="<?php echo esc_url(get_template_directory_uri('template_url'));?>/js/plugins/selectivizr.js"></script>
        <![endif]-->
		<?php wp_footer(); ?>
		
    </body>
</html>