<?php
define('DOMAIN' , 'wp_paperplane');

define('SH_NAME', 'wp_paperplane');

define('SH_VERSION', 'v1.0');

define('SH_ROOT', get_template_directory().'/');

define('SH_URL', get_template_directory_uri().'/');

include_once('includes/loader.php');
add_action('after_setup_theme', 'sh_theme_setup');

function sh_theme_setup()
{

	global $wp_version;
	
	load_theme_textdomain(SH_NAME, get_template_directory() . '/languages');

	add_editor_style();

	//ADD THUMBNAIL SUPPORT

	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('menus');
	add_theme_support( "custom-header");
	add_theme_support( "custom-background");
	add_theme_support( "title-tag" ) ;
	
	/** Register wp_nav_menus */

	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'header_menu' => __('Main Menu', SH_NAME),
			)
		);
	}
	
	if ( ! isset( $content_width ) ) 
	
		$content_width = 960;

	add_image_size( '68x68', 68, 68, true );
		
	add_image_size( '180x45', 180, 45, true );
	
	add_image_size( '262x385', 262, 385, true );
	
	add_image_size( '555x315', 555, 315, true );
	
	add_image_size( '845x530', 845, 530, true );
	
	add_image_size( '1115x422', 1115, 422, true );
}


function sh_widget_init()
{

	register_widget("TW_latest_posts");
	

	global $wp_registered_sidebars;

	

	register_sidebar(array(

	  'name' => __( 'Default Sidebar', SH_NAME ),

	  'id' => 'default-sidebar',

	  'description' => __( 'Widgets in this area will be shown on the right-hand side.', SH_NAME ),

	  'class'=>'',

	  'before_widget'=>'<div class="paper_plane_widget widget %s">',

	  'after_widget'=>'</div>',

	  'before_title' => '<div class="widget-title">',

	  'after_title' => '</div>'

	));
	



	$sidebars = sh_set(sh_set( get_option(SH_NAME.'_theme_options'), 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 



	foreach( array_filter((array)$sidebars) as $sidebar)

	{

		if(sh_set($sidebar , 'topcopy')) break ;

		$slug = sh_slug( $sidebar ) ;

		

		register_sidebar( array(

			'name' => sh_set($sidebar , 'sidebar_name'),

			'id' =>  sh_set($slug , 'sidebar_name') ,

			'before_widget'=>'<div class="widget %s">',

			'after_widget'=>'</div>',
			
			'before_title' => '<div class="widget-title">',
			
			'after_title' => '</div>',

		) );		

	}

	

	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;

}

add_action( 'widgets_init', 'sh_widget_init' );