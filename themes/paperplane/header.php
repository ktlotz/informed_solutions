<?php 
	$theme_options = _WSH()->option();
	$layout = sh_set($theme_options , 'theme_layouts');
	if(!is_user_logged_in() && sh_set($theme_options , 'coming_soon_template')){
		get_template_part( 'coming-soon/coming-soon' );
		exit;
	}
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />
<meta name="GOOGLEBOT" content="index follow" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<?php if ( sh_set( $theme_options, 'site_favicon' ) ) : ?>
		<link rel="icon" type="image/png" href="<?php echo esc_url(sh_set( $theme_options, 'site_favicon' )); ?>">
<?php endif; ?>
<!-- =========================================
CSS
========================================== -->
<?php wp_head(); ?>
</head><!-- /head -->

<!-- =========================================
body
========================================== -->
<body <?php body_class($layout.' coming-soon'); ?>>
<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
<![endif]-->
<?php if(sh_set($theme_options , 'cs_preloader')): ?>
<!-- =========================================
Loader
========================================== -->
<div id="loader">
	<div id="loader-container">
		<img src="<?php echo esc_url(sh_set($theme_options , 'cs_preloader_image')); ?>" alt="<?php esc_attr_e("Loading..." , SH_NAME) ; ?>" />
	</div>
</div>
<?php endif; ?>
<!-- =========================================
Menu
========================================== -->
<!-- nav-wrapper -->
<nav id="nav-wrapper">
	<!-- navbar -->
	<div class="navbar">

		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">

				<!-- col-md-12 -->
				<div class="col-md-12">

					<!-- navbar-header -->
					<div class="navbar-header">

						<!-- Mobile Menu -->
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
							<i class="fa fa-bars"></i>
						</button>
						<!-- / mobile Menu -->
						
						<?php
							$one_page_sections = sh_set( sh_set($theme_options , 'one_page_section') , 'one_page_section' ) ;
							$one_page_links = sh_set( sh_set($theme_options , 'one_page_links') , 'one_page_links' ) ;
							$home_link = home_url('/');
							$data_sroll= '';
							if(sh_set($theme_options , 'site_logo')):
							 if(sh_set($theme_options , 'show_one_page_settings')){
								 if((is_home() || is_front_page()) && !($wp_query->is_posts_page)){
									 $data_sroll= 'data-scroll';
									 if(sh_set($theme_options , 'cs_slider'))
									 {
									 	$home_link = '#home-section';
									 }
									 elseif( count($one_page_sections) > 1 && !(sh_set($theme_options , 'cs_slider')) )
									 {
										 
										 $home_link = '#sec_'.sh_set(sh_set($one_page_sections , 0) , 'section_page');
									 }
								 }
							 }
						?>
						<!-- logo -->		
						<a href="<?php echo esc_url($home_link); ?>" class="navbar-brand" <?php echo esc_attr($data_sroll); ?> title="<?php esc_attr_e("Logo" , SH_NAME); ?>">
						
							<img src="<?php echo esc_url(sh_set($theme_options , 'site_logo')); ?>" alt="<?php esc_attr_e("Logo" , SH_NAME); ?>" style="height:<?php echo esc_attr(sh_set($theme_options , 'logo_height')); ?>px !important; width:<?php echo esc_attr(sh_set($theme_options , 'logo_width')); ?>px !important;" />
						</a><!-- /logo -->
						<?php endif; ?>

					</div><!-- /navbar-header -->

					<!-- navbar-collapse -->
					<?php 
						if(!(sh_set($theme_options , 'show_one_page_settings'))):
							wp_nav_menu(
									array(  
										'theme_location'=> 'header_menu', 
										'menu_class'=>'nav navbar-nav navbar-right' ,
										'menu_id' => '' , 
										'container' =>'div' , 
										'container_class'=> 'navbar-collapse collapse',
										'depth' => 1,
									)
							); 
						endif;
						
						if(sh_set($theme_options , 'show_one_page_settings')):
					?>
					
					<div class="navbar-collapse collapse">

						<!-- nav -->
						<ul class="nav navbar-nav navbar-right">
						
						<?php
							global $wp_query;
							if((is_home() || is_front_page()) && !($wp_query->is_posts_page)):
							
							if(sh_set($theme_options , 'cs_slider') && sh_set($theme_options , 'slider_menu_name')):
						?>
								<li>
									<a href="#home-section" title="<?php echo esc_attr(sh_set($theme_options , 'slider_menu_name')) ; ?>" data-scroll class="scrollto">
										<?php echo esc_html(sh_set($theme_options , 'slider_menu_name')) ; ?>
									</a>
								</li>
						<?php 
							endif;
														
							foreach($one_page_sections as $one_page_section):
								if(sh_set($one_page_section , 'tocopy')) break;
						?>
							<li>
								<a href="#sec_<?php echo esc_attr(sh_set($one_page_section , 'section_page')); ?>" title="<?php echo esc_attr( sh_set($one_page_section , 'menu_name')); ?>" data-scroll class="scrollto">
									<?php echo esc_html(sh_set($one_page_section , 'menu_name')) ; ?>
								</a>
							</li>
						<?php
							endforeach;
							
							foreach($one_page_links as $one_page_link):
								if(sh_set($one_page_link , 'tocopy')) break;
						?>
							<li>
								<a href="<?php echo esc_url(sh_set($one_page_link , 'menu_link')); ?>" title="<?php echo esc_attr(sh_set($one_page_link , 'menu_links_name')) ; ?>">
									<?php echo esc_html(sh_set($one_page_link , 'menu_links_name')); ?>
								</a>
							</li>
							
						<?php
							endforeach;
							endif;
						?>
						
						<?php if(!(is_home() || is_front_page()) || ($wp_query->is_posts_page)): ?>
							<li>
								<a href="<?php echo esc_url(home_url());?>" title="<?php esc_attr_e("Home" , SH_NAME);?>">
									<i class="fa fa-reply-all"></i> 
									<?php esc_html_e(" Back To Home" , SH_NAME);?>
								</a>
							</li>
						<?php endif ; ?>
						
						</ul><!-- /nav -->

					</div><!-- /navbar-collapse -->
					<script>
					jQuery(document).ready(function(e) {
						
						jQuery('body').attr('data-spy', 'scroll').attr('data-target', '#nav-wrapper').attr('data-offset', '100');
						jQuery('a.scrollto').click(function (event) {
							event.preventDefault();
							if (jQuery('.navbar-collapse').hasClass('in')) {
								jQuery('.navbar-collapse').removeClass('in').addClass('collapse');
							}
						});
						
						jQuery('.navbar-toggle').click(function (event) {
							jQuery('.navbar-collapse').toggleClass('nav-visable');
						});
						
						jQuery('.navbar-collapse').click(function (event) {
							jQuery('.navbar-collapse').removeClass('nav-visable');
							jQuery('.navbar-collapse').removeClass('in').addClass('collapse');
						});
						
						smoothScroll.init({
							offset: 90,
							speed: 600,
							updateURL: false
						});
					
					});
					</script>
					<?php endif; ?>

				</div><!-- /col-md-12 -->

			</div><!-- /row -->
		</div><!-- /container -->

	</div><!-- /navbar -->
</nav><!-- /nav-wrapper -->

<?php
	if((is_home() || is_front_page()) && !($wp_query->is_posts_page)):
		if(sh_set($theme_options , 'slides_no') > 0):
			if(sh_set($theme_options , 'cs_slider')):
?>
		<div <?php if(sh_set($theme_options , 'theme_layouts')=='parallax-image'):?>style="background-image: url(<?php echo esc_url(sh_set($theme_options , 'parallax_image_bg')); ?>);"<?php endif; ?> id="home-section">
			<div id="home-section-overlayer"></div>
			<div id="owl-home-wrapper">
				<div class="owl-home">
					<?php 
					  $count = 0;
					  $args = array('post_type' => 'sh_slider' , 'posts_per_page' => sh_set($theme_options , 'slides_no') ) ; 
					  query_posts($args);
					  if(have_posts()):  while(have_posts()): the_post(); 
					  global $post ; 
					  $meta = get_post_meta(get_the_ID() , 'slider_meta' , true);
					?>
					
					<?php if(sh_set($meta , 'slide_layout')=='right_image'): ?>
					<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
						<div class="slide-content">
							<div class="col-md-5">
								<div class="slide-title">
									<?php echo esc_html(sh_set($meta , 'slide_text')); ?>
								</div>
								<div class="slide-desc">
									<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
								</div>
								<div class="slide-divider"></div>
								<div class="slide-button">
									<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')) ; ?>" class="btn btn-meduim btn-nesto">
										<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
									</a>
								</div>
							</div>
							<div class="col-md-7">
								<div class="feature-left-image">
									<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
								</div>
							</div>
						</div>
					</div>
					<?php endif;?>
					
					<?php if(sh_set($meta , 'slide_layout')=='left_image'): ?>
					<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
						<div class="slide-content">
							<div class="col-md-7">
								<div class="feature-left-image">
									<img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
								</div>
							</div>
							<div class="col-md-5">
								<div class="slide-title">
									<?php echo esc_html(sh_set($meta , 'slide_text')); ?>
								</div>
								<div class="slide-desc">
									<?php echo esc_html(sh_set($meta , 'slide_description')); ?>
								</div>
								<div class="slide-divider"></div>
								<div class="slide-button">
									<a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
										<?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php endif;?>
					
					<?php if(sh_set($meta , 'slide_layout')=='top_image'): ?>
					<div class="slide-item" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
                        <div class="slide-content">
                            <div class="slide-logo">
                                <img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
                            </div>
                            <div class="slide-title">
                               <?php echo esc_html(sh_set($meta , 'slide_text')); ?>
                            </div>
                            <div class="slide-desc">
                                <?php echo esc_html(sh_set($meta , 'slide_description')); ?>
                            </div>
                            <div class="slide-divider"></div>
                            <div class="slide-button">
                                <a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
                                    <?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
                                </a>
                            </div>
                        </div>
                    </div>
					<?php endif;?>
					
					<?php if(sh_set($meta , 'slide_layout')=='bottom_image'): ?>
					<div class="slide-item no-bottom-padding" <?php if(sh_set($theme_options , 'theme_layouts') == 'parallax-slider'):?>style="background-image: url(<?php echo esc_url(sh_set($meta , 'slide_background_image')); ?>)"<?php endif;?>>
                        <div class="slide-content">
                            <div class="slide-title">
                                <?php echo esc_html(sh_set($meta , 'slide_text')); ?>
                            </div>
                            <div class="slide-desc">
                                <?php echo esc_html(sh_set($meta , 'slide_description')); ?>
                            </div>
                            <div class="slide-divider"></div>
                            <div class="slide-button">
                                <a href="<?php echo esc_url(sh_set($meta , 'slide_button_link')); ?>" title="<?php echo esc_attr(sh_set($meta , 'slide_button_text')); ?>" class="btn btn-meduim btn-nesto">
                                    <?php echo esc_html(sh_set($meta , 'slide_button_text')); ?>
                                </a>
                            </div>
                            <div class="feature-bottom-image">
                                <img src="<?php echo esc_url(sh_set($meta , 'slide_main_image')); ?>" alt="" />
                            </div>
                        </div>
                    </div>
					<?php endif;?>
					
					<?php
						$count++;
					  endwhile ; endif ;
					  wp_reset_query();
					?>
				</div>
				
				<?php if( $count > 1 ) :?>
				<div class="owl-navigation">
					<a class="home-prev"><i class='fa fa-angle-left'></i></a>
					<a class="home-next"><i class='fa fa-angle-right'></i></a>
				</div>
				<?php endif; ?>
				
			</div>
		</div>
		<?php if(sh_set($theme_options , 'theme_layouts') == 'video-background'): ?>
		<?php wp_enqueue_script( 'okvideo' ); ?>
		<script>
			jQuery(document).ready(function($) {
				if (jQuery('body').hasClass('video-background')) {
						/* ==========================================================================
						okvideo
						========================================================================== */
						jQuery(function () {
							jQuery('.video-background #home-section').okvideo({
								hd: true,
								volume: 0,
								loop: true,
								adproof: true,
								autoplay: true,
								annotations: false,
								source: '<?php echo esc_js(sh_set($theme_options , 'video_background_link')); ?>'
							});
						});
					}
			});
		</script>
		<?php endif; ?>
<?php		endif;
		endif;
	endif;
?>