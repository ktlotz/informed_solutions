<?php
class SH_Enqueue

{


	function __construct()

	{

		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );

		add_action( 'wp_head', array( $this, 'wp_head' ) );

		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

	}


	function sh_enqueue_scripts()
	{
	  global $post;
	  $theme_options = _WSH()->option();
	  $color_scheme = (sh_set($theme_options , 'custom_color_scheme')) ? sh_set($theme_options , 'custom_color_scheme') : '' ;
	  $theme_style = 'style' ;
	  $theme_style = (sh_set($theme_options , 'custom_theme_style')=='angled') ? 'angled' : 'style' ;
		
	  $protocol = is_ssl() ? 'https' : 'http';
		$styles = array( 
					'main_style' => 'style.css',
					'pe-icon-7-stroke' => 'fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
					'fonts_karma' => $protocol.'://fonts.googleapis.com/css?family=Karma:400,700',
					'fonts_montserrat' => $protocol.'://fonts.googleapis.com/css?family=Montserrat:400,700',
					'font-awesome.min' => 'fonts/fontawesome/css/font-awesome.min.css' ,
					'owl.carousel' => 'js/plugins/owl-carousel/owl.carousel.css' ,
					'owl.theme' =>'js/plugins/owl-carousel/owl.theme.css' ,
					'owl.transitions' => 'js/plugins/owl-carousel/owl.transitions.css' ,
					'jquery.fancybox' => 'js/plugins/fancybox/jquery.fancybox.css',
					'animate.min' => 'js/plugins/wow/animate.min.css',
					'bootstrap.min' => 'css/bootstrap.min.css',
					'bootstrap' => 'css/bootstrap.css',
					'style'=>'css/'.$theme_style.'.css',
					
				 );
		if(!(strstr(sh_set($theme_options , 'custom_color_scheme'),"custom_color_scheme_black")) && sh_set($theme_options , 'custom_color_scheme')){
				$styles['color'] = get_template_directory_uri().'/css/'.$color_scheme.'.css' ;
		}
		foreach( $styles as $name => $style )
		{

			if(strstr($style, 'http') || strstr($style, 'https') ) 
			{
				wp_enqueue_style( $name, $style);
			}

			else wp_enqueue_style( $name, SH_URL.$style);
		}

		$scripts = array( 
						  'modernizr.custom' => 'js/vendor/modernizr.custom.js',
						  'bootstrap.min' => 'js/vendor/bootstrap.min.js', 
						  'jquery.placeholder' => 'js/plugins/jquery.placeholder.js',
						  'smooth-scroll.min' => 'js/plugins/smoothscroll/smooth-scroll.min.js',
						  'jquery.easing.1.3' => 'js/plugins/jquery.easing.1.3.js',
						  'owl.carousel.min' => 'js/plugins/owl-carousel/owl.carousel.min.js',
						  'jquery.parallax-1.1.3'=>'js/plugins/jquery.parallax-1.1.3.js',
						  'jquery.countTo'=>'js/plugins/jquery.countTo.js',
						  'progress'=>'js/plugins/progress.js',
						  'waypoints.min'=>'js/plugins/waypoints/waypoints.min.js',
						  'jquery.fancybox.pack'=>'js/plugins/fancybox/jquery.fancybox.pack.js',
						  'jquery.fancybox-media'=>'js/plugins/fancybox/helpers/jquery.fancybox-media.js',
						  'wow.min'=>'js/plugins/wow/wow.min.js',
						  'okvideo'=>'js/plugins/okvideo/okvideo.min.js',
						  'lwtCountdown'=>'js/plugins/jquery.lwtCountdown-1.0.js',
						  'PaperPlane'=>'js/PaperPlane.js',
						 );
		foreach( $scripts as $name => $js )
		{
			wp_register_script( $name, SH_URL.$js, '', '', true);
		}
		
		wp_enqueue_script( array(
							'jquery' ,
							'modernizr.custom',
							'bootstrap.min',
							'jquery.placeholder' ,
							'smooth-scroll.min',
							'jquery.easing.1.3',
							'owl.carousel.min',
							'jquery.parallax-1.1.3',
							'jquery.countTo',
							'waypoints.min',
							'progress',
							'jquery.fancybox.pack',
							'jquery.fancybox-media',
							'wow.min',
							'PaperPlane',
						   )
						);
		
		if( is_singular() ) 
			wp_enqueue_script('comment-reply');
		if( is_page_template( 'tpl-compare.php' ) ) 
			wp_enqueue_script( array( 'jquery-prettyPhoto' ) );
		if( is_singular( 'product' ) ) 
			wp_enqueue_script( array( 'jquery-prettyPhoto', 'owl.carousel.min' ) );
		
		if( is_single() ) {
			$format = get_post_format();
			if( $format == 'gallery' ) wp_enqueue_script( array( 'jquery-flexslider' ) );
			if( $format == 'video' ) wp_enqueue_script( array( 'jquery-fitvids' ) );
			wp_enqueue_style('like-styles', get_template_directory_uri().'/css/like-styles.css');
		}
		
		wp_enqueue_script( array('custom_script', 'theme_scripts') );
	}
	function wp_head()
	{
		//wp_enqueue_script( array('jquery'));
		$options = get_option(SH_NAME.'_theme_options');
		echo  sh_set($options , 'custom_css');
	}
	
	function wp_footer()

	{

		$this->sh_enqueue_scripts();

	}

}