<?php


/// Latest posts
class TW_latest_posts extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'TW_latest_posts', /* Name */__('Latest Posts ',SH_NAME), array( 'description' => __('Show the Latest posts with images', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses($before_widget , array('div' => array('class' => array()))); ?>
		
		<div class="latest-posts-widget">
		<?php 
		
		echo wp_kses($before_title , array('div' => array('class' => array()))).wp_kses($title , array('div' => array('class' => array()))).wp_kses($after_title , array('div' => array('class' => array()))); 
		
		$query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
	
		$this->posts();
		
	?>


                </div>
		
		<?php wp_reset_query();  ?>
		<?php echo wp_kses($after_widget , array('div' => array('class' => array())));
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Featured Posts', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_html($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
       
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>__('All Categories', SH_NAME), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>        
            <div class="widget-content">
                
                <?php while( have_posts() ): the_post(); ?>
				
				<div class="latest-post">

					<!-- post-image -->
					<div class="post-image">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('68x68'); ?>
						</a>
					</div><!-- /post-image -->

					<!-- post-title -->
					<div class="post-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_title(); ?>
						</a>
					</div><!-- /post-title -->

				</div>
                <?php endwhile; ?>
                
            </div>
            
		<?php endif;
    }
}
