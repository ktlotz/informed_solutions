<?php
	ob_start();
	$img_src = sh_set(wp_get_attachment_image_src($left_image , '555x315') , 0);
	$features = explode(',' , $points);
	$padding_bottom = ( $bottom_padding ) ? 'about-section-3' : 'about-section-1' ;
?>
<div id="<?php echo esc_attr($padding_bottom) ; ?>">

	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<!-- col-md-6 -->
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s">

				<!-- short-section-title -->
				<div class="short-section-title wow">
					<?php echo esc_html($title) ;?>
				</div><!-- /short-section-title -->

				<!-- section-content -->
				<div class="section-content">
					<?php echo esc_html($text1) ;?>
				</div><!-- /section-content -->

				<!-- section-list -->
				<div class="section-list">
					<ul>
					<?php foreach((array)$features as $feature): ?>
					<li><?php echo esc_html($feature) ; ?></li>
					<?php endforeach; ?>
					</ul>
				</div><!-- /section-list -->

				<!-- section-content -->
				<div class="section-content">
					<?php echo esc_html($text2) ;?>
				</div><!-- /section-content -->

			</div><!-- /col-md-6 -->

			<!-- col-md-6 -->
			<div class="col-md-6 wow fadeInRight" data-wow-duration="1.5s">

				<!-- feature-right-image -->
				<div class="feature-right-image">
					<img src="<?php echo esc_url($img_src); ?>" alt="<?php esc_attr_e("Feature Image",SH_NAME);?>" />
				</div><!-- /feature-right-image -->

			</div><!-- /col-md-6 -->

		</div><!-- /row -->
	</div><!-- /container -->

</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>