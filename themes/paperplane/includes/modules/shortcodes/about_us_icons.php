<?php
	ob_start();
	$img_src = sh_set(wp_get_attachment_image_src($right_image , '555x315') , 0);
	$padding_bottom = ( $bottom_padding ) ? 'about-section-3' : 'about-section-2' ;
?>
<div id="<?php echo esc_attr($padding_bottom) ; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s">
				<div class="feature-left-image">
					<img src="<?php echo esc_url($img_src) ; ?>" alt="" />
				</div>
			</div>
			<div class="col-md-6 wow fadeInRight" data-wow-duration="1.5s">
				<div class="short-section-title">
					<?php echo esc_html($title) ; ?>
				</div>
				<div class="section-content">
					<?php echo esc_html($text) ; ?>
				</div>
				<?php if($feature_1): ?>
				<div class="feature-box-style-1">
					<div class="feature-icon">
						<i class="<?php echo esc_html($feature_1_icon); ?>"></i>
					</div>
					<div class="feature-content">
						<?php echo esc_html($feature_1); ?>
					</div>
				</div>
				<?php
					endif;
					if($feature_2):
				?>
				<div class="feature-box-style-1">
					<div class="feature-icon">
						<i class="<?php echo esc_html($feature_2_icon); ?>"></i>
					</div>
					<div class="feature-content">
						<?php echo esc_html($feature_2) ; ?>
					</div>
				</div>
				<?php
					endif;
					if($feature_3):
				?>
				<div class="feature-box-style-1">
					<div class="feature-icon">
						<i class="<?php echo esc_html($feature_3_icon); ?>"></i>
					</div>
					<div class="feature-content">
						<?php echo esc_html($feature_3) ; ?>
					</div>
				</div>
				<?php
					endif;
				?>
			</div>
		</div>
	</div>
</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>