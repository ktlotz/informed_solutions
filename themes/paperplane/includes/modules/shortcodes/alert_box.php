<?php ob_start(); ?>

<?php if($tagline || $button_text): ?>
<div id="alert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="short-section-title">
					<?php echo esc_html($tagline) ; ?>
				</div>
			</div>
			<div class="col-md-3">
				<?php if($button_text):?>
				<a href="<?php echo esc_url($button_link) ; ?>" title="<?php echo esc_attr($button_text) ; ?>" class="btn btn-meduim btn-nesto">
					<?php echo esc_html($button_text) ; ?>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>