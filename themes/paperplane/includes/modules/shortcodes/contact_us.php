<?php ob_start(); ?>
<div id="contact-section">
	<div class="container">
		<div class="row">
		
			<?php if($title): ?>
			<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">

				<!-- section-title -->
				<div class="section-title">
					<?php echo esc_html($title) ; ?>
				</div><!-- /section-title -->

			</div>
			<?php endif; ?>
			
			<div class="col-md-12 wow pulse" data-wow-duration="1.5s">
				<div class="row">
				
					<form class="contact-form" id="contactform" method="post" action="<?php echo esc_url(admin_url('admin-ajax.php?action=_sh_ajax_callback&amp;subaction=sh_contact_form_submit')); ?>">
						<div class="msgs"></div>
						
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?php esc_attr_e("Name" , SH_NAME); ?>" name="contact_name" id="name" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?php esc_attr_e("Email" , SH_NAME); ?>" name="contact_email" id="email" required>
							</div>

						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?php esc_attr_e("Subject" , SH_NAME); ?>" name="contact_subject" id="subject">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<textarea rows="10" class="form-control" placeholder="<?php esc_attr_e("Message" , SH_NAME); ?>" name="contact_message" id="message" required></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" id="contact_form_submit" class="btn btn-lg btn-nesto-o" name="contact_submit">
								<?php echo ($button_text) ? esc_html($button_text) : __( "Send Now", SH_NAME ) ; ?>
							</button>
						</div>
					</form>
					
				</div>
			</div>
			
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($) {
	$('#contactform').live('submit', function(e){
	
		e.preventDefault();
		var thisform = this;
		var fields = $(this).serialize();
		var url= $(this).attr('action');
		//alert(url);
		$.ajax({
			url: url,
			type: 'POST',
			data: fields,
			success:function(res){
				//salert(res);
				$('.msgs', thisform).html(res);
			}
		});
	});
	

});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>