<?php
	ob_start();
	$img_src = sh_set(wp_get_attachment_image_src($left_image , '1115x422') , 0);
	$bg_img_src = sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0);
?>
<div id="download-section" style="background-image: url(<?php echo esc_url($bg_img_src ); ?>);">

	<!-- download-section-overlayer -->
	<div id="download-section-overlayer">


		<!-- col-md-4 -->
		<div class="col-md-4">

			<!-- download-content -->
			<div class="download-content">

					<!-- short-section-title -->
					<div class="short-section-title">
						<?php echo esc_html($title) ; ?>
					</div><!-- /short-section-title -->

					<!-- section-content -->
					<div class="section-content">
						<?php echo esc_html($text) ; ?>
					</div><!-- /section-content -->

					<a href="<?php echo esc_url($button_link); ?>" title="<?php echo esc_attr($button_text) ; ?>" class="btn btn-meduim btn-nesto">
						<?php echo esc_html($button_text) ; ?>
					</a>

			</div><!-- /download-content -->

		</div><!-- /col-md-4 -->


		<!-- col-md-8 -->
		<div class="col-md-8">

			<!-- feature-bottom-image -->
			<div class="feature-bottom-image">
				<img src="<?php echo esc_url($img_src); ?>" alt="" />
			</div><!-- /feature-bottom-image -->

		</div><!-- /col-md-8 -->


	</div><!-- /download-section-overlayer -->

</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>