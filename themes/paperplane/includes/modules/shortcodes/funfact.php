<?php
	ob_start();
	$num = is_numeric($number);
	if($num):
?>
<div class="col-md-3 col-sm-3">

	<!-- number-box -->
	<div class="number-box">

		<!-- box-numbers -->
		<div class="box-numbers">
			<span data-to="<?php echo esc_html($number) ; ?>">0</span><?php if($plus) esc_html_e('+' , SH_NAME) ; ?>
		</div><!-- /box-numbers -->

		<!-- box-title -->
		<div class="box-title">
			<?php echo esc_html($title) ;?>
		</div><!-- /box-title -->

	</div><!-- /number-box -->

</div>
<?php
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ;
?>