<?php
	ob_start();
	$img_src = esc_url(sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0));
?>
<div style="background-image: url(<?php echo esc_url($img_src); ?>);" id="numbers-section">

	<!-- numbers-section-overlayer -->
	<div id="numbers-section-overlayer">

		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				
			<?php echo do_shortcode($contents); ?>

			</div><!-- /row -->
		</div><!-- /container -->

	</div><!-- /numbers-section-overlayer -->

</div>
<script>
jQuery(document).ready(function($) {
	 jQuery('.box-numbers [data-to]').each(function () {
        var $this = jQuery(this);
        $this.waypoint(function () {
            $this.countTo({speed: 3000});
        }, {offset: '100%', triggerOnce: true });
    });
});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>