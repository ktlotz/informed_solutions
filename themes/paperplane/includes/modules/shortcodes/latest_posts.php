<?php ob_start() ; ?>
<div id="blog-section">

	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<?php if($title): ?>
			<!-- col-md-12 -->
			<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">

				<!-- section-title -->
				<div class="section-title">
					<?php echo esc_html($title) ; ?>
				</div><!-- /section-title -->

			</div><!-- /col-md-12 -->
			<?php endif; ?>
			
			<?php 
				$count = 1 ;
				$args = array('post_type' => 'post') ;
				if( $cat ) 
				$args['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat));
				query_posts($args);
				if(have_posts()):  
				while(have_posts()): the_post(); 
				global $post ;
				if(!$number<1):
			?>
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s">
				<div class="recent-post">
					
					<?php if ( has_post_thumbnail() ): ?>
					<div class="post-image">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail('555x315', array( 'class' => 'img-responsive' )); ?>
						</a>
					</div>
					<?php endif; ?>

					<div class="post-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_title(); ?>
						</a>
					</div>

					<div class="post-meta">
						<span>
							<?php echo get_the_date('j'); ?>
						</span>
						<span>
							<?php echo get_the_date('M'); ?>
						</span>
					</div>

					<div class="post-content">
						<?php echo esc_html(substr(get_the_excerpt(), 0 , 250 )); ?>
					</div>

					<div class="post-button">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-meduim btn-nesto-o">
							<?php esc_html_e("Read More" , SH_NAME ) ; ?>
						</a>
					</div>

				</div>


			</div>
			
			<?php
				endif;
				if($count == $number) break;
				$count++;
				endwhile ; endif ;
				wp_reset_query();
			?>
			
			<?php if($button_text): ?>
			<!-- col-md-12 -->
			<div class="col-md-12">

				<!-- btn-more -->
				<div class="btn-more">
					<a href="<?php echo esc_url($button_link); ?>" title="<?php echo esc_attr($button_text); ?>" class="btn btn-meduim btn-nesto-o">
						<?php echo esc_html($button_text) ; ?>
					</a>
				</div><!-- /btn-more -->

			</div><!-- /col-md-12 -->
			<?php endif; ?>


		</div><!-- /row -->
	</div><!-- /container -->

</div>
<script>
jQuery(document).ready(function($) {
	jQuery(this).find('.post-image img').each(function () {
        jQuery(this).addClass('grayscale');
    });
});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>