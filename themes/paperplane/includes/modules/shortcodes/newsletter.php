<?php
	ob_start();
	$bg_img_src = sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0);
?>
<div style="background-image: url(<?php echo esc_url($bg_img_src); ?>);" id="subscribe-section">
	<div id="subscribe-section-overlayer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<?php echo esc_html($title) ; ?>
					</div>
				</div>
				<div class="col-md-12">
					<form id="newsletter-form" role="form" action="http://feedburner.google.com/fb/a/mailverify" accept-charset="utf-8" method="post">
						<div class="row">
							<div class="col-md-9">
								<div class="form-group">
									<input type="email" class="form-control" placeholder="<?php esc_attr_e("Enter your e-mail address" , SH_NAME) ; ?>" name="email" value="">
								</div>
							</div>
							<input type="hidden" id="uri" name="uri" value="<?php echo esc_attr($f_id) ; ?>">
							<input type="hidden" value="en_US" name="loc">
							<div class="col-md-3">
								<div class="form-group">
									<button type="submit" class="btn btn-meduim btn-nesto">
										<?php echo ($button_text) ? esc_html($button_text) : esc_html( "Subscribe Now", SH_NAME ) ; ?>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>