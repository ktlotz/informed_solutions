<?php
	ob_start();
	$theme_options = _WSH()->option();
	$partners = sh_set(sh_set($theme_options , 'partners') , 'partners');
	if(count($partners)>1 && !($number<=0) ):
?>
<div id="clients-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 wow pulse" data-wow-duration="1.5s">
				<div id="owl-client-wrapper">
					<div class="owl-client">

						<?php
							$count = 1;
							foreach($partners as $partner):
								if(sh_set($partner , 'tocopy')) break;
								$img_id = sh_get_attachment_id_by_url(sh_set($partner , 'image'));
								$img_src = sh_set(wp_get_attachment_image_src( $img_id , '180x45') , 0);
						?>
						<!-- slide-item -->
						<div>

							<!-- slide-content -->
							<div class="slide-content">
								<a href="<?php echo esc_url(sh_set($partner , 'image_link')); ?>" title="<?php echo esc_attr(sh_set($partner , 'image_title')); ?>" data-rel="tooltip" data-placement="top">
									<img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr(sh_set($partner , 'image-title')); ?>" />
								</a>
							</div><!-- /slide-content -->

						</div><!-- /slide-item -->
						<?php
							if($count==$number) break;
							else $count++;
							endforeach;
						?>


					</div><!-- /owl-client -->

					<?php if($count>4): ?>
					<!-- owl-navigation -->
					<div class="owl-navigation">
						<a class="client-prev"><i class='fa fa-angle-left'></i></a>
						<a class="client-next"><i class='fa fa-angle-right'></i></a>
					</div><!-- /owl-navigation -->
					<?php endif; ?>

				</div><!-- /owl-client-wrapper -->

			</div><!-- /col-md-12 -->


		</div><!-- /row -->
	</div><!-- /container -->

</div>
<script>
jQuery(document).ready(function($) {
	jQuery(this).find('.client-img img').each(function () {
        jQuery(this).addClass('grayscale');
    });
	jQuery('.owl-client').owlCarousel({
        items: 4,
        autoPlay: true,
        stopOnHover: true,
        pagination: false,
        navigation: false,
        itemsDesktop : [1199, 4],
        itemsDesktopSmall : [992, 3],
        itemsTablet: [768, 2],
        itemsTabletSmall: [480, 1],
        itemsMobile : [479, 1]
    });

    jQuery('a.client-next').click(function () {
        jQuery('.owl-client').trigger('owl.next');
    });
    jQuery('a.client-prev').click(function () {
        jQuery('.owl-client').trigger('owl.prev');
    });
});
</script>
<?php 
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>