<?php
	ob_start();
	$theme_options = _WSH()->option();
	$portfolios = sh_set(sh_set($theme_options , 'portfolios') , 'portfolios');
	$check_number = $portfolio_column*2;
	
	$count = 1 ;
	if($number > 0):
?>
<div id="portfolio-section">

	<!-- owl-portfolio-wrapper -->
	<div id="owl-portfolio-wrapper">

		<!-- owl-portfolio -->
		<div class="owl-portfolio grid-<?php echo esc_attr($portfolio_column); ?>">

			<!-- slide-item -->
			<div class="slide-item">
				<?php
				foreach($portfolios as $portfolio):
				
				$img_id = sh_get_attachment_id_by_url(sh_set($portfolio , 'portfolio_image'));
					
					if($portfolio_column==2){ $img_src = sh_set(wp_get_attachment_image_src( $img_id , array(960,600)) , 0); }
					
					if($portfolio_column==3){ $img_src = sh_set(wp_get_attachment_image_src( $img_id , array(500,313)) , 0); }
					
					if($portfolio_column==4){ $img_src = sh_set(wp_get_attachment_image_src( $img_id , array(400,500)) , 0); }
				?>
				<!-- portfolio-content -->
				<div class="portfolio-content">

					<!-- Portfolio Image -->
					<img src="<?php echo esc_url($img_src);?>" alt="<?php echo esc_attr(sh_set($portfolio , 'portfolio_name'));?>"/>

					<!-- portfolio-details -->
					<div class="portfolio-details">

						<!-- portfolio-info -->
						<div class="portfolio-info">

							<span>
								<?php echo esc_html(sh_set($portfolio , 'portfolio_name')) ; ?>
							</span>
							<span>
								<a href="<?php echo esc_url(sh_set($portfolio , 'portfolio_link'));?>" title="<?php esc_attr_e("Link" , SH_NAME); ?>">
									<i class="pe-7s-paperclip"></i>
								</a>
							</span>
							
							<?php if(sh_set($portfolio , 'portfolio_pop_up')== 'video'): ?>
							<span>
								<a class="fancybox-media" href="<?php echo esc_url(sh_set($portfolio , 'popup_video_link'));?>" title="<?php echo esc_attr(sh_set($portfolio , 'portfolio_name'));?>">
									<i class="pe-7s-search"></i>
								</a>
							</span>
							<?php endif;
								if(sh_set($portfolio , 'portfolio_pop_up') != 'video'):
							?>
							<span>
								<a class="fancybox" href="<?php echo esc_url(sh_set($portfolio , 'portfolio_image'));?>" title="<?php echo esc_attr(sh_set($portfolio , 'portfolio_name'));?>">
									<i class="pe-7s-search"></i>
								</a>
							</span>
							<?php endif; ?>

						</div><!-- /portfolio-info -->

					</div><!-- /portfolio-details -->

				</div>
				
		<?php if($count%$check_number==0 && ($count != $number)): ?>
			</div>
			<div class="slide-item">			
		<?php endif; ?>
		
		<!-- /portfolio-content -->
		<?php
			if($count == $number) break; 
			$count++;
			endforeach;
		?>
			</div><!-- /slide-item -->

		</div><!-- /owl-portfolio -->

		<?php if($count > $check_number): ?>
		
		<!-- owl-navigation -->
		<div class="owl-navigation">
			<a class="portfolio-prev"><i class='fa fa-angle-left'></i></a>
			<a class="portfolio-next"><i class='fa fa-angle-right'></i></a>
		</div><!-- /owl-navigation -->
		
		<?php endif;?>

	</div><!-- /owl-portfolio-wrapper -->


</div>
<script>
jQuery(document).ready(function($) {
	
	jQuery('.owl-portfolio').owlCarousel({
        items: 1,
        autoPlay: true,
        singleItem: true,
        stopOnHover: true,
        pagination: false,
        navigation: false
    });
	
	jQuery(this).find('.portfolio-content img').each(function () {
        jQuery(this).addClass('grayscale');
    });
	
	jQuery('.fancybox').fancybox({
        helpers : {
            title: {
                type: 'over'
            }
        }
    });
	
    jQuery('.fancybox-media').fancybox({
        helpers : {
            media : {},
            overlay : {
                speedOut : 0,
                locked: false
            }
        }
    });
});
</script>
<?php
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>