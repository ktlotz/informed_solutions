<?php 
	ob_start();
	$features = explode(',' , $fetures); 
	$theme_options = _WSH()->option();
	$feature_number = sh_set($theme_options , 'feature_number');
?>
<div class="col-md-4 wow fadeInLeft" data-wow-duration="1.5s">

	<!-- pricing-table -->
	<div class="pricing-table">

		<!-- table-price -->
		<div class="table-price">
			<?php
				echo esc_html($price) ; 
			?>
			<span><?php echo esc_html($currency) ; ?><?php if($currency && $price_duration): ?>/<?php endif; ?><?php echo esc_html($price_duration); ?></span>
		</div><!-- /table-price -->

		<!-- table-title -->
		<div class="table-title">
			<?php echo esc_html($title) ; ?>
		</div><!-- /table-title -->

		<!-- table-desc -->
		<div class="table-desc">
			<?php echo esc_html($text) ; ?>
		</div><!-- /table-desc -->

		<!-- table-feature -->
		<div class="table-feature">
			<ul>
				<?php for($i=0 ; $i<$feature_number ; $i++): ?>
					<li><?php echo (sh_set($features , $i)) ? esc_html(sh_set($features , $i)) :'X' ; ?></li>
				<?php endfor ; ?>
			</ul>
		</div><!-- /table-feature -->

		<!-- table-button -->
		<div class="table-button">
			<a href="<?php echo esc_url($button_link ); ?>" title="<?php echo esc_attr($button_text) ; ?>">
				<?php echo ($button_text) ? esc_html($button_text) : esc_html("Buy Now" , SH_NAME) ; ?>
			</a>
		</div><!-- /table-button -->

	</div><!-- /pricing-table -->


</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ;
?>
