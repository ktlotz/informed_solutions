<?php
	ob_start();
	if($title && $text):
?>
<div class="col-md-4 wow fadeInLeft" data-wow-duration="1.5s">

	<!-- feature-box-style-3 -->
	<div class="feature-box-style-3">

		<!-- feature-icon -->
		<div class="feature-icon">
			<i class="<?php echo esc_html($icon);?>"></i>
		</div><!-- /feature-icon -->

		<!-- feature-number -->
		<div class="feature-number">
			<?php echo esc_html($number) ; ?>
		</div><!-- /feature-number -->

		<!-- feature-title -->
		<div class="feature-title">
			<?php echo esc_html($title) ; ?>
		</div><!-- /feature-title -->

		<!-- feature-content -->
		<div class="feature-content">
			<?php echo esc_html($text) ; ?>
		</div><!-- /feature-content -->

	</div><!-- /feature-box-style-3 -->

</div>
<?php
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ;
?>