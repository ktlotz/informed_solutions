<?php ob_start(); ?>
<div id="process-section">

	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">


			<!-- col-md-12 -->
			<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">

				<!-- section-title -->
				<div class="section-title">
					<?php echo esc_html($title) ; ?>
				</div>

			</div><!-- /col-md-12 -->

			<?php echo do_shortcode($contents); ?>
			
		</div><!-- /row -->
	</div><!-- /container -->

</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>

