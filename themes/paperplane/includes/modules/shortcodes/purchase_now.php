<?php
	ob_start();
	$img_src = sh_set(wp_get_attachment_image_src($b_image , array(1140,233)) , 0);
	
	$bg_img_src = sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0);
?>
<div style="background-image: url(<?php echo esc_url($bg_img_src); ?>);" id="purchase-section">

	<!-- purchase-section-overlayer -->
	<div id="purchase-section-overlayer">

		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">


				<!-- col-lg-7 col-md-12 -->
				<div class="col-lg-7 col-md-12">

					<!-- short-section-title -->
					<div class="short-section-title">
						<?php echo esc_html($title) ; ?>
					</div><!-- /short-section-title -->

					<!-- section-content -->
					<div class="section-content">
						<?php echo esc_html($text) ; ?>
					</div><!-- /section-content -->

				</div><!-- /col-lg-7 col-md-12 -->

				<?php if($button_text1 || $button_text2): ?>
				<!-- col-lg-5 col-md-12 -->
				<div class="col-lg-5 col-md-12">

					<!-- section-content -->
					<div class="section-content">
						<?php if($button_text1): ?>
						<a href="<?php echo esc_url($button_link1); ?>" class="btn btn-medium btn-nesto">
							<?php echo esc_html($button_text1) ; ?>
						</a>
						<?php 
							endif; 
							
							if($button_text2):
						?>
						<a href="<?php echo esc_url($button_link2); ?>" class="btn btn-medium btn-nesto">
							<?php echo esc_html($button_text2) ; ?>
						</a>
						<?php endif; ?>
					</div><!-- /section-content -->
				</div>
				<!-- /col-lg-5 col-md-12 -->
				<?php endif; ?>
				
				<?php if($b_image): ?>
				<!-- col-md-12 -->
				<div class="col-md-12">

					<!-- feature-bottom-image -->
					<div class="feature-bottom-image">
						<img src="<?php echo esc_url($img_src); ?>" alt="<?php esc_attr_e("Image",SH_NAME);?>" />
					</div><!-- /feature-bottom-image -->

				</div>
				<!-- /col-md-12 -->
				<?php endif; ?>

			</div><!-- /row -->
		</div><!-- /container -->

	</div><!-- /purchase-section-overlayer -->

</div>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>
