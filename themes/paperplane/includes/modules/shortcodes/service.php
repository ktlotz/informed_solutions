<?php ob_start(); ?>
<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s">

	<!-- feature-box-style-2 -->
	<div class="feature-box-style-2">

		<!-- feature-icon -->
		<div class="feature-icon">
			<i class="<?php echo esc_html($icon);?>"></i>
		</div><!-- /feature-icon -->

		<!-- feature-title -->
		<div class="feature-title">
			<?php echo esc_html($title) ; ?>
		</div><!-- /feature-title -->

		<!-- feature-content -->
		<div class="feature-content">
			<?php echo esc_html($text) ; ?>
		</div><!-- /feature-content -->

	</div><!-- /feature-box-style-2 -->

</div>
<?php
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ;
 ?>