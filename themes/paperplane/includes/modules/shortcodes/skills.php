<?php
	ob_start();
	$theme_options = _WSH()->option();
	$skills = sh_set(sh_set($theme_options , 'skill') , 'skill');
?>
<div id="skills-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">
				<div class="section-title">
					<?php echo esc_html($title) ; ?>
				</div>
			<?php
				$count = 1 ;
				foreach($skills as $skill):
					if(sh_set($skill , 'tocopy')) break;
					$skill_number = ($count==1)?'skill':'skill-'.$count ;
					if($count<=$number):
			?>
				<div class="col-md-3 col-sm-6 wow fadeInLeft" data-wow-duration="1.5s">
					<div class="skill-box">
						<div class="skill-value">
							<canvas id="<?php echo esc_html($skill_number) ; ?>" class="skill" data-rel="<?php echo esc_html(sh_set($skill , 'skill_percentage'));?>"></canvas>
							<span><?php echo esc_html(sh_set($skill , 'skill_percentage')) ; ?></span>
						</div>
						<div class="skill-title">
							<?php echo esc_html(sh_set($skill , 'skill_name')) ; ?>
						</div>
						<div class="skill-desc">
							<?php echo esc_html(sh_set($skill , 'skill_description')) ; ?>
						</div>
					</div>
				</div>
			<?php
					endif;
				$count++;
				endforeach;
			?>
		</div>
		</div>
	</div>
</div>

<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>