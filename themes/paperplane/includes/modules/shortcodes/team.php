<?php
	ob_start() ;
?>
<div id="team-section">

	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">


			<!-- col-md-12 -->
			<div class="col-md-12 wow fadeInDown" data-wow-duration="1.5s">

				<!-- section-title -->
				<div class="section-title">
					<?php echo esc_html($title) ; ?>
				</div>

			</div><!-- /col-md-12 -->
			<?php 
			  $args = array('post_type' => 'sh_team' , 'posts_per_page' => $number ) ; 
			  if( $cat ) 
				$args['tax_query'] = array(array('taxonomy' => 'team_category','field' => 'id','terms' => $cat));
			  query_posts($args);
			  if(have_posts()):  while(have_posts()): the_post(); 
			  global $post ; 
			  $meta = get_post_meta(get_the_ID() , 'team_meta' , true);
			  $social_media = sh_set($meta , 'icons');
		  	?>
			<div class="col-md-3 col-sm-6 wow fadeInLeft" data-wow-duration="1.5s">


				<!-- team-box -->
				<div class="team-box">

					<!-- member-header -->
					<div class="member-header">

						<!-- member-image -->
						<div class="member-image">
							<?php the_post_thumbnail('262x385'); ?>
						</div><!-- /member-image -->

						<!-- member-social -->
						<div class="member-social">
							<ul>
								<?php
									foreach($social_media as $team_link):
									$icon = str_replace( "icon" , "fa" , sh_set($team_link , 'fontawesome') );
								?>
								<li>
									<a href="<?php echo esc_url(sh_set($team_link , 'link')); ?>" title="<?php echo esc_attr(sh_set($team_link , 'title')); ?>">
										<i class="fa <?php echo esc_attr($icon); ?>"></i>
									</a>
								</li>
								<?php endforeach; ?>
							</ul>
						</div><!-- /member-social -->

					</div><!-- /member-header -->

					<!-- member-name -->
					<div class="member-name">
						<?php the_title(); ?>
					</div><!-- /member-name -->

					<!-- member-title -->
					<div class="member-title">
						<?php echo esc_html(sh_set($meta , 'description')) ; ?>
					</div><!-- /member-title -->

				</div><!-- /team-box -->


			</div>
			<?php 
			  endwhile ; endif ;
			  wp_reset_query();
		  	?>
		</div><!-- /row -->
	</div><!-- /container -->

</div>
<script>
jQuery(document).ready(function($) {
	jQuery(this).find('.team-box img').each(function () {
        jQuery(this).addClass('grayscale');
    });
});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>