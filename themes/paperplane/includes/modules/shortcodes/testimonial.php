<?php
	ob_start();
	$bg_img_src = sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0);
	$theme_options = _WSH()->option();
	$testimonials = sh_set(sh_set($theme_options , 'testimonials') , 'testimonials' ) ;
	if($number > 0):
		$count = 1;
?>
<div style="background-image: url(<?php echo esc_url($bg_img_src) ; ?>);" id="testimonials-section">
	<div id="testimonials-section-overlayer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="owl-testimonials-wrapper">
						<div class="owl-testimonials">
							<?php
								foreach($testimonials as $testimonial):
								if(sh_set($testimonial , 'tocopy')) break;
								$img_id = sh_get_attachment_id_by_url(sh_set($testimonial , 'testimonial_image'));
								$img_src = sh_set(wp_get_attachment_image_src( $img_id , array(125,125)) , 0);
							?>
							<div>
								<div class="slide-content">
									<?php if(sh_set($testimonial , 'testimonial_image')): ?>
									<div class="client-img">
										<img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr(sh_set($testimonial , 'testimonial_name')) ; ?>" />
									</div>
									<?php endif; ?>
									<div class="client-name">
										<?php echo esc_html(sh_set($testimonial , 'testimonial_name')) ; ?>
									</div>
									<div class="client-quote">
										<?php echo esc_html(sh_set($testimonial , 'testimonial_text')) ; ?>
									</div>
								</div>
							</div>
							<?php
								if($count == $number) break;
								$count++;
								endforeach;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($) {
	 jQuery('.owl-testimonials').owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: true,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true
    });
	jQuery(this).find('.client-img img').each(function () {
        jQuery(this).addClass('grayscale');
    });
});
</script>
<?php 
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>