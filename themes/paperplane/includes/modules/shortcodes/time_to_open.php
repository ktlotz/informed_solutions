<?php
	ob_start();
	wp_enqueue_script( 'lwtCountdown' );
?>
<div id="countdown-section">
	<div class="container">
		<div class="row">
			<?php if($title): ?>
			<div class="col-md-12">
				<div class="section-title">
					<i class="fa fa-clock-o fa-spin"></i> <?php echo esc_html($title) ; ?>
				</div>
			</div>
			<?php endif;?>
			<div class="col-md-12">
				<div id="countdown_dashboard">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="dash days_dash">
							<div class="digit">0</div>
							<div class="digit">0</div>
							<div class="digit">0</div>
							<span class="dash_title"><?php esc_html_e("Days" , SH_NAME); ?></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="dash hours_dash">
							<div class="digit">0</div>
							<div class="digit">0</div>
							<span class="dash_title"><?php esc_html_e("Hours" , SH_NAME); ?></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="dash minutes_dash">
							<div class="digit">0</div>
							<div class="digit">0</div>
							<span class="dash_title"><?php esc_html_e("Minutes" , SH_NAME); ?></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="dash seconds_dash">
							<div class="digit">0</div>
							<div class="digit">0</div>
							<span class="dash_title"><?php esc_html_e("Seconds" , SH_NAME); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($) {
	if (jQuery('body').hasClass('coming-soon')) {
        jQuery('#countdown_dashboard').countDown({
            targetDate: {
                'day': <?php echo esc_js($date_to_open); ?>,
                'month': <?php echo esc_js($month_to_open); ?>,
                'year': <?php echo esc_js($year_to_open); ?>,
                'hour': 0,
                'min': 0,
                'sec': 0
            },
            omitWeeks: true
        });
    }
});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>