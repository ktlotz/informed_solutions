<?php
	ob_start() ;
	$img_src = sh_set(wp_get_attachment_image_src($bg_image , 'full') , 0);
	$video_src = str_replace( "vimeo.com" , "player.vimeo.com/video" , $link );
	if($link):
?>
<div style="background-image: url(<?php echo esc_url($img_src); ?>);" id="video-section">

	<!-- video-section-overlayer -->
	<div id="video-section-overlayer">

		<div class="video-player">
		   <iframe src="<?php echo esc_url($video_src); ?>"></iframe>
		</div>

		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">


				<!-- col-md-12 -->
				<div class="col-md-12">

					<!-- play-video -->
					<a href="javascript:void(0)" title="Play Video" class="play-video">
						<i class="pe-7s-play"></i>
					</a><!-- /play-video -->

					<!-- stop-video -->
					<a href="javascript:void(0)" title="Stop Video" class="stop-video">
						<i class="pe-7s-close"></i>
					</a><!-- /stop-video -->

					<!-- short-section-title -->
					<div class="short-section-title">
						<?php echo esc_html($title) ; ?>
					</div><!-- /short-section-title -->

				</div><!-- /col-md-12 -->

			</div><!-- /row -->
		</div><!-- /container -->

	</div><!-- /video-section-overlayer -->

</div>
<script>
jQuery(document).ready(function($) {
	
	// Open Video
	 jQuery('.play-video').on('click', function (e) {
        jQuery('.play-video').css({display: 'none'});
        jQuery('.stop-video').css({display: 'block'});
        jQuery('.video-player').css({display: 'block'});
        e.preventDefault();
    });

    // Close Video
    jQuery('.stop-video').on('click', function (e) {
        jQuery('.stop-video').css({display: 'none'});
        jQuery('.video-player').css({display: 'none'});
        jQuery('.play-video').css({display: 'block'});
        e.preventDefault();
    });
});
</script>
<?php 
	endif;
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>