<?php

$theme_option = _WSH()->option() ; 
$team_slug = sh_set($theme_option , 'team_permalink' , 'sh_team') ;
$slider_slug = sh_set($theme_option , 'slider_permalink' , 'sh_slider') ;

$options = array();

$options['sh_team'] = array(
							'labels' => array(__('Team', SH_NAME), __('Team', SH_NAME)),
							'slug' => 'sh_team' ,
							'label_args' => array('menu_name' => __('Team', SH_NAME)),
							'supports' => array( 'title' , 'thumbnail' ),
							'label' => __('Team', SH_NAME),
							'args'=>array(
										'menu_icon'=>get_template_directory_uri().'/images/team.png' , 
										'taxonomies'=>array('team_category')
									)
					 );

$options['sh_slider'] = array(
							'labels' => array(__('Slide', SH_NAME), __('Slides', SH_NAME)),
							'slug' => 'sh_slider' ,
							'label_args' => array('menu_name' => __('Slider', SH_NAME)),
							'supports' => array( 'title'),
							'label' => __('Slider', SH_NAME),
							'args'=>array(
										'menu_icon'=>get_template_directory_uri().'/images/post_slider.png' , 
									)
					 );