<?php
$sh_sc = array();

$sh_sc['sh_about_us_bullets'] 	= array(
									"name" => __("About Us Left Image", SH_NAME),
									"base" => "sh_about_us_bullets",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Bullets Description with left side image.', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Text 1", SH_NAME),
													   "param_name" => "text1",
													   "description" => __("Enter text before Features", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Text 2", SH_NAME),
													   "param_name" => "text2",
													   "description" => __("Enter text after Features", SH_NAME)
													),
													array(
													   "type" => "exploded_textarea",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Points", SH_NAME),
													   "param_name" => "points",
													   "description" => __("1 Point per Line", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Left Image", SH_NAME),
													   "param_name" => "left_image",
													   "description" => __("Upload Image", SH_NAME)
													),
													array(
													   "type" => "checkbox",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Bottom Padding ?", SH_NAME),
													   "param_name" => "bottom_padding",
													   'value' => array('Bottom Padding' => 1 ),
													   "description" => __("check if you want to add bottom padding of 30 px after this shortcode", SH_NAME)
													),
												)
						 		  );
$sh_sc['sh_about_us_icons'] 	= array(
									"name" => __("About Us Right Image", SH_NAME),
									"base" => "sh_about_us_icons",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Bullets Description with left side image.', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Text", SH_NAME),
													   "param_name" => "text",
													   "description" => __("Enter text before Features", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 1", SH_NAME),
													   "param_name" => "feature_1",
													   "description" => __("Enter text for Feature 1", SH_NAME)
													),
													array(
													   "type" => "dropdown",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 1 Icon", SH_NAME),
													   "param_name" => "feature_1_icon",
													   "value" => array_flip( (array)tw_theme_icons() ),
													   "description" => __("Choose Icon for Feature", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 2", SH_NAME),
													   "param_name" => "feature_2",
													   "description" => __("Enter text for Feature 2", SH_NAME)
													),
													array(
													   "type" => "dropdown",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 2 Icon", SH_NAME),
													   "param_name" => "feature_2_icon",
													   "value" => array_flip( (array)tw_theme_icons() ),
													   "description" => __("Choose Icon for Feature", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 3", SH_NAME),
													   "param_name" => "feature_3",
													   "description" => __("Enter text for Feature 3", SH_NAME)
													),
													array(
													   "type" => "dropdown",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feature 3 Icon", SH_NAME),
													   "param_name" => "feature_3_icon",
													   "value" => array_flip( (array)tw_theme_icons() ),
													   "description" => __("Choose Icon for Feature", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Right Image", SH_NAME),
													   "param_name" => "right_image",
													   "description" => __("Upload Image", SH_NAME)
													),
													array(
													   "type" => "checkbox",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Bottom Padding ?", SH_NAME),
													   "param_name" => "bottom_padding",
													   'value' => array('Bottom Padding' => 1 ),
													   "description" => __("check if you want to add bottom padding of 30 px after this shortcode", SH_NAME)
													),
												)
						 		  );
$sh_sc['sh_download_app_now'] 	= array(
									"name" => __("Download App Now", SH_NAME),
									"base" => "sh_download_app_now",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Download App Now Section', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Text", SH_NAME),
													   "param_name" => "text",
													   "description" => __("Enter text for the Services.", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Left Image", SH_NAME),
													   "param_name" => "left_image",
													   "description" => __("Upload Image", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Background Image", SH_NAME),
													   "param_name" => "bg_image",
													   "description" => __("Upload Section Background Image", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Button Text", SH_NAME),
													   "param_name" => "button_text",
													   "description" => __("Enter text for Button.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Button Link", SH_NAME),
													   "param_name" => "button_link",
													   "description" => __("Enter Link for Button.", SH_NAME)
													),
													
												)
						  );
$sh_sc['sh_partner'] 	= array(
							"name" => __("Partners", SH_NAME),
							"base" => "sh_partner",
							"class" => "",
							"category" => __('PaperPlane', SH_NAME),
							"icon" => 'icon-clock-o' ,
							'description' => __('Partners Section.', SH_NAME),
							"params" => array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Number", SH_NAME),
											   "param_name" => "number",
											   "description" => __("Enter number of partner to show.", SH_NAME)
											),
										),
							);	
$sh_sc['sh_video']  	= array(
							"name" => __("Video", SH_NAME),
							"base" => "sh_video",
							"class" => "",
							"category" => __('PaperPlane', SH_NAME),
							"icon" => 'icon-clock-o' ,
							'description' => __('Video Section.', SH_NAME),
							"params" => array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Title", SH_NAME),
											   "param_name" => "title",
											   "description" => __("Enter title for Video Section.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Link", SH_NAME),
											   "param_name" => "link",
											   "description" => __("Enter Vimeo Link", SH_NAME)
											),
											array(
											   "type" => "attach_image",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Background Image", SH_NAME),
											   "param_name" => "bg_image",
											   "description" => __("Upload Background Image", SH_NAME)
											),
										)
						  );
				
$sh_sc['sh_services']	=	array(
								"name" => __("Services Section", SH_NAME),
								"base" => "sh_services",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_parent" => array('only' => 'sh_service'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Section of Services.', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title for this Section", SH_NAME)
												),
											),
								"js_view" => 'VcColumnView'
							);
$sh_sc['sh_service']	=	array(
								"name" => __("Service", SH_NAME),
								"base" => "sh_service",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_child" => array('only' => 'sh_services'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Service.', SH_NAME),
								"params" => array(
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Icon", SH_NAME),
												   "param_name" => "icon",
												   "value" => array_flip( (array)tw_theme_icons() ),
												   "description" => __("Choose Icon for Service", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title of Service", SH_NAME)
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Text", SH_NAME),
												   "param_name" => "text",
												   "description" => __("Enter Text", SH_NAME)
												),
			
											),
							);	
$sh_sc['sh_processes']	=	array(
								"name" => __("Processes Section", SH_NAME),
								"base" => "sh_processes",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_parent" => array('only' => 'sh_process'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Section of Processes.', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title for this Section", SH_NAME)
												),
											),
								"js_view" => 'VcColumnView'
							);
$sh_sc['sh_process']	=	array(
								"name" => __("Process", SH_NAME),
								"base" => "sh_process",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_child" => array('only' => 'sh_processes'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Process.', SH_NAME),
								"params" => array(
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Icon", SH_NAME),
												   "param_name" => "icon",
												   "value" => array_flip( (array)tw_theme_icons() ),
												   "description" => __("Choose Icon for Process", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title of Process", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Number", SH_NAME),
												   "param_name" => "number",
												   "description" => __("Give a Number for Process", SH_NAME)
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Text", SH_NAME),
												   "param_name" => "text",
												   "description" => __("Enter Process", SH_NAME)
												),
			
											),
							);
							
$sh_sc['sh_funfacts']	=	array(
								"name" => __("FunFacts Section", SH_NAME),
								"base" => "sh_funfacts",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_parent" => array('only' => 'sh_funfact'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Section of FunFacts.', SH_NAME),
								"params" => array(
												array(
												   "type" => "attach_image",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Background Image", SH_NAME),
												   "param_name" => "bg_image",
												   "description" => __("Upload Background Image", SH_NAME)
												),
											),
								"js_view" => 'VcColumnView'
							);
$sh_sc['sh_funfact']	=	array(
								"name" => __("Fact", SH_NAME),
								"base" => "sh_funfact",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_child" => array('only' => 'sh_funfacts'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Fact.', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title of Fact", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Number", SH_NAME),
												   "param_name" => "number",
												   "description" => __("Number of Fact", SH_NAME)
												),
												array(
												   "type" => "checkbox",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("show + ?", SH_NAME),
												   "param_name" => "plus",
												   'value' => array('Show Plus Sign' => 1 ),
												   "description" => __("check if you want to show Plus sign after Numbers.", SH_NAME)
												),
											),
							);
$sh_sc['sh_team']	=	array(
							"name" => __("Team", SH_NAME),
							"base" => "sh_team",
							"class" => "",
							"category" => __('PaperPlane', SH_NAME),
							"icon" => 'icon-clock-o' ,
							'description' => __('Team Section', SH_NAME),
							"params" => array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Title", SH_NAME),
											   "param_name" => "title",
											   "description" => __("Enter title for Team Section.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Number", SH_NAME),
											   "param_name" => "number",
											   "description" => __("Number of Team Member to show.", SH_NAME)
											),
											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __( 'Category', SH_NAME ),
											   "param_name" => "cat",
											   "value" => array_flip(sh_get_categories( array( 
																				'taxonomy' => 'team_category',
																				'hide_empty' => false,)
																					) ),
											   "description" => __( 'Choose Category.', SH_NAME )
											),
											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Order By", SH_NAME),
											   "param_name" => "sort",
											   'value' => array_flip(
															array(
																'date'=>__('Date', SH_NAME),
																'title'=>__('Title', SH_NAME) ,
																'name'=>__('Name', SH_NAME) ,
																'author'=>__('Author', SH_NAME),
																'comment_count' =>__('Comment Count', SH_NAME),
																'random' =>__('Random', SH_NAME) 
																) ),			
											   "description" => __("Enter the sorting order.", SH_NAME)
											),
											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Order", SH_NAME),
											   "param_name" => "order",
											   'value' => array(
															'ASC'=>__('Ascending', SH_NAME),
															'DESC'=>__('Descending', SH_NAME) 
														  ),			
											   "description" => __("Enter the Order.", SH_NAME)
											),
							)
						);
$sh_sc['sh_purchase_now'] 	= array(
									"name" => __("Purchase It Now", SH_NAME),
									"base" => "sh_purchase_now",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Purchase It Now Section', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Text", SH_NAME),
													   "param_name" => "text",
													   "description" => __("Enter Text for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("1st Button Text", SH_NAME),
													   "param_name" => "button_text1",
													   "description" => __("Enter text for Button.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("1st Button Link", SH_NAME),
													   "param_name" => "button_link1",
													   "description" => __("Enter Link for Button.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("2nd Button Text", SH_NAME),
													   "param_name" => "button_text2",
													   "description" => __("Enter text for Button.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("2nd Button Link", SH_NAME),
													   "param_name" => "button_link2",
													   "description" => __("Enter Link for Button.", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Bottom Image", SH_NAME),
													   "param_name" => "b_image",
													   "description" => __("Upload Image", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Background Image", SH_NAME),
													   "param_name" => "bg_image",
													   "description" => __("Upload Image", SH_NAME)
													),
												)
						  );
$sh_sc['sh_skills']	=	array(
							"name" => __("Skills", SH_NAME),
							"base" => "sh_skills",
							"class" => "",
							"category" => __('PaperPlane', SH_NAME),
							"icon" => 'icon-clock-o' ,
							'description' => __('SKills Section', SH_NAME),
							"params" => array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Title", SH_NAME),
											   "param_name" => "title",
											   "description" => __("Enter title for Skills Section.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Number", SH_NAME),
											   "param_name" => "number",
											   "description" => __("Number of Skills to show.", SH_NAME)
											),
										)
						);
$sh_sc['sh_portfolio']	=	array(
								"name" => __("Portfolio", SH_NAME),
								"base" => "sh_portfolio",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								'description' => __('Portfolio Section', SH_NAME),
								"params" => array(
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Column", SH_NAME),
												   "param_name" => "portfolio_column",
												   'value' => array(
																'2'=>'2',
																'3'=>'3',
																'4'=>'4', 
															  ),			
												   "description" => __("Select Column.", SH_NAME),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Number", SH_NAME),
												   "param_name" => "number",
												   "description" => __("Number of Portfolio Images to show.", SH_NAME)
												),
											)
							);
$sh_sc['sh_pricing_tables']	=	array(
								"name" => __("Pricing Tables Section", SH_NAME),
								"base" => "sh_pricing_tables",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_parent" => array('only' => 'sh_pricing_table'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Section of Pricing Tables.', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title for this Section", SH_NAME)
												),
											),
								"js_view" => 'VcColumnView'
							);
$sh_sc['sh_pricing_table']	=	array(
								"name" => __("Pricing Table", SH_NAME),
								"base" => "sh_pricing_table",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								"as_child" => array('only' => 'sh_pricing_tables'),
								"content_element" => true,
								"show_settings_on_create" => true,
								'description' => __('Add Pricing Table.', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title of Pricing Table", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Text", SH_NAME),
												   "param_name" => "text",
												   "description" => __("Enter Title of Pricing Table", SH_NAME)
												),
												array(
												   "type" => "exploded_textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Features", SH_NAME),
												   "param_name" => "fetures",
												   "description" => __("One Feature per Line(IF LINE EXCEED THE FEATURE NUMBER THEN IT WOULD NOT BE SHOWN.)", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Price", SH_NAME),
												   "param_name" => "price",
												   "description" => __("Enter Price of Pakage", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Currency", SH_NAME),
												   "param_name" => "currency",
												   "description" => __("Enter Currency of Price", SH_NAME)
												),
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Price Duration", SH_NAME),
												   "param_name" => "price_duration",
												   'value' => array(
																'Hour'=>'h',
																'Day'=>'d',
																'Week'=>'w', 
																'Month' => 'm',
																'Year' => 'y',
															  ),			
												   "description" => __("", SH_NAME),
												   "default"=>"Month",
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Button Text", SH_NAME),
												   "param_name" => "button_text",
												   "description" => __("Enter Button Text", SH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Button Link", SH_NAME),
												   "param_name" => "button_link",
												   "description" => __("Enter Button Link", SH_NAME)
												),
											),
							);
$sh_sc['sh_newsletter'] 	= array(
									"name" => __("Newsletter", SH_NAME),
									"base" => "sh_newsletter",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Newsletter Section', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Button Text", SH_NAME),
													   "param_name" => "button_text",
													   "description" => __("Enter Text for Button.", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Background Image", SH_NAME),
													   "param_name" => "bg_image",
													   "description" => __("Upload Image", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Feed Burner ID", SH_NAME),
													   "param_name" => "f_id",
													   "description" => __("Enter Feed Burner ID.", SH_NAME)
													),
												)
						  );
$sh_sc['sh_latest_posts']	= array(
								"name" => __("Latest Posts", SH_NAME),
								"base" => "sh_latest_posts",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								'description' => __('Latest Posts Section.', SH_NAME),
								"params"=>array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Title", SH_NAME),
											   "param_name" => "title",
											   "description" => __("Enter title for this Section.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Number", SH_NAME),
											   "param_name" => "number",
											   "description" => __("Enter Number of post to show.", SH_NAME)
											),

											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __( 'Category', SH_NAME ),
											   "param_name" => "cat",
											   "value" => array_flip( 
											   sh_get_categories( array( 
																	'taxonomy' => 'category',
																	'hide_empty' => false)
																	) ),
											   "description" => __( 'Choose Category.', SH_NAME )
											),
											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Order By", SH_NAME),
											   "param_name" => "sort",
											   'value' => array_flip( array(
																		'date'=>__('Date', SH_NAME),
																		'title'=>__('Title', SH_NAME) ,
																		'name'=>__('Name', SH_NAME) ,
																		'author'=>__('Author', SH_NAME),
																		'comment_count' =>__('Comment Count', SH_NAME),
																		'random' =>__('Random', SH_NAME) 
																		) ),			
											   "description" => __("Enter the sorting order.", SH_NAME)
											),
											array(
											   "type" => "dropdown",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Order", SH_NAME),
											   "param_name" => "order",
											   'value' => array(
															'ASC'=>__('Ascending', SH_NAME),
															'DESC'=>__('Descending', SH_NAME) 
														  ),			
											   "description" => __("Enter the Order.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Button Text", SH_NAME),
											   "param_name" => "button_text",
											   "description" => __("Enter Button Text.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Button Link", SH_NAME),
											   "param_name" => "button_link",
											   "description" => __("Enter Blog Page Link", SH_NAME)
											),
										)
							  );
$sh_sc['sh_testimonial'] 	= array(
									"name" => __("Testimonials", SH_NAME),
									"base" => "sh_testimonial",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Testimonials Section', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Number", SH_NAME),
													   "param_name" => "number",
													   "description" => __("Number of Skills to show.", SH_NAME)
													),
													array(
													   "type" => "attach_image",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Background Image", SH_NAME),
													   "param_name" => "bg_image",
													   "description" => __("Upload Image", SH_NAME)
													),
												)
						  );
$sh_sc['sh_contact_us'] 	= array(
									"name" => __("Contact US", SH_NAME),
									"base" => "sh_contact_us",
									"class" => "",
									"category" => __('PaperPlane', SH_NAME),
									"icon" => 'icon-clock-o' ,
									'description' => __('Contact US Section', SH_NAME),
									"params" => array(
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Title", SH_NAME),
													   "param_name" => "title",
													   "description" => __("Enter title for the this section.", SH_NAME)
													),
													array(
													   "type" => "textfield",
													   "holder" => "div",
													   "class" => "",
													   "heading" => __("Button Text", SH_NAME),
													   "param_name" => "button_text",
													   "description" => __("Enter Text for the Button.", SH_NAME)
													),
												)
						  );
$sh_sc['sh_alert_box'] 	= array(
							"name" => __("Alert Box", SH_NAME),
							"base" => "sh_alert_box",
							"class" => "",
							"category" => __('PaperPlane', SH_NAME),
							"icon" => 'icon-clock-o' ,
							'description' => __('Alert Box Section', SH_NAME),
							"params" => array(
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Tagline", SH_NAME),
											   "param_name" => "tagline",
											   "description" => __("Enter Tagline", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Button Text", SH_NAME),
											   "param_name" => "button_text",
											   "description" => __("Enter text for Button.", SH_NAME)
											),
											array(
											   "type" => "textfield",
											   "holder" => "div",
											   "class" => "",
											   "heading" => __("Button Link", SH_NAME),
											   "param_name" => "button_link",
											   "description" => __("Enter Link for Button.", SH_NAME)
											),
										)
						  );
$sh_sc['sh_time_to_open'] 	= array(
								"name" => __("Time to Open", SH_NAME),
								"base" => "sh_time_to_open",
								"class" => "",
								"category" => __('PaperPlane', SH_NAME),
								"icon" => 'icon-clock-o' ,
								'description' => __('Time to Open Section', SH_NAME),
								"params" => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Title", SH_NAME),
												   "param_name" => "title",
												   "description" => __("Enter Title for this section", SH_NAME)
												),
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Date", SH_NAME),
												   "param_name" => "date_to_open",
												   'value' => array(
												   				'1'=>'1',
																'2'=>'2',
																'3'=>'3',
																'4'=>'4',
																'5'=>'5',
																'6'=>'6',
																'7'=>'7',
																'8'=>'8',
																'9'=>'9',
																'10'=>'10',
																'11'=>'11',
																'12'=>'12',
																'13'=>'13',
																'14'=>'14',
																'15'=>'15',
																'16'=>'16',
																'17'=>'17',
																'18'=>'18',
																'19'=>'19',
																'20'=>'20',
																'21'=>'21',
																'22'=>'22',
																'23'=>'23',
																'24'=>'24',
																'25'=>'25',
																'26'=>'26',
																'27'=>'27',
																'28'=>'28',
																'29'=>'29',
																'30'=>'30',
																'31'=>'31', 
															  ),			
												   "description" => __("Select Date", SH_NAME),
												),
												array(
												   "type" => "dropdown",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Month", SH_NAME),
												   "param_name" => "month_to_open",
												   'value' => array(
												   				'1'=>'1',
																'2'=>'2',
																'3'=>'3',
																'4'=>'4',
																'5'=>'5',
																'6'=>'6',
																'7'=>'7',
																'8'=>'8',
																'9'=>'9',
																'10'=>'10',
																'11'=>'11',
																'12'=>'12',															  ),			
												   "description" => __("Select Month", SH_NAME),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __("Year", SH_NAME),
												   "param_name" => "year_to_open",
												   "description" => __("Enter Year.", SH_NAME)
												),
											)
							  );
			
$sh_sc = apply_filters( '_sh_shortcodes_array', $sh_sc );
	
return $sh_sc;