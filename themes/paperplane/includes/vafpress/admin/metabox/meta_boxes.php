<?php
$options = array();

$options[] =  array(
	'id'          => 'page_meta',
	'types'       => array('page'),
	'title'       => __('Page Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
						array(
							'type' => 'toggle',
							'name' => 'show_breadcrum',
							'label' => __('show Breadcrum ?', SH_NAME),
							'default' => '',
							'description' => ''
						),
						array(
							'type' => 'toggle',
							'name' => 'show_comment',
							'label' => __('show Comments ?', SH_NAME),
							'default' => '',
							'description' => ''
						),
						array(
							'type' => 'upload',
							'label' => __('Top Image', SH_NAME),
							'name'  => 'image',
							'default' => '',
							'description' => __('Uplaod Cover Photo for title background if you want to use tour theme as multipage.', SH_NAME),
						),
					),
				);

/* Post options */
$options[] =  array(
				'id'          => 'post_meta',
				'types'       => array('post'),
				'title'       => __('Post Settings', SH_NAME),
				'priority'    => 'high',
				'template'    =>  array(
									array(
										'type' => 'toggle',
										'label' => __('Breadcrum ?', SH_NAME),
										'name'  => 'breadcrum',
									),
									array(
										'type' => 'upload',
										'label' => __('Top Image', SH_NAME),
										'name'  => 'image',
										'default' => '',
										'description' => __('Uplaod Cover Photo', SH_NAME),
									),
									array(
										'type' => 'select',
										'label' => __('Sidebar', SH_NAME),
										'name'  => 'sidebar',
										'default' => '',
										'description' => __('Sidebar', SH_NAME),
										'items' =>  sh_get_sidebars('multi' == true) ,
									),
								  ),
				);

// Team Options
$options[] =  array(
				'id'          => 'team_meta',
				'types'       => array( 'sh_team' ),
				'title'       => __('Team Settings', SH_NAME),
				'priority'    => 'high',
				'template'    => array(
									array(
										'type' => 'textbox',
										'name' => 'description',
										'label' => __('Description', SH_NAME),
										'default' => '',
									),
									array(
										'type'      => 'group',
										'repeating' => true,
										'sortable'  => true,
										'name'      => 'icons',
										'title'     => __('Social Icons', SH_NAME),
										'fields'    =>  array(
															array(
																'type' => 'fontawesome',
																'name' => 'fontawesome',
																'label' => __('Social Icon', SH_NAME),
																'default' => '',
															),
															array(
																'type' => 'textbox',
																'name' => 'title',
																'label' => __('Social Icon Title', SH_NAME),
																'default' => '',
															),
															array(
																'type' => 'textbox',
																'name' => 'link',
																'label' => __('Social Icon Link', SH_NAME),
																'default' => '',
															),
														),
									),
								 ),
			  );
			  
// Slider Options
$options[] =  array(
				'id'          => 'slider_meta',
				'types'       => array( 'sh_slider' ),
				'title'       => __('Slide Settings', SH_NAME),
				'priority'    => 'high',
				'template'    => array(
									array(
										'type' => 'select',
										'name' => 'slide_layout',
										'label' => __('Slide Layout', SH_NAME),
										'description' => __('Select Slide Layout.', SH_NAME),
										'items' =>  array(
														array(
															'value' => 'right_image',
															'label' => __('Slide With Right Image', SH_NAME),
														),
														array(
															'value' => 'left_image',
															'label' => __('Slide with Left Image', SH_NAME),
														),
														array(
															'value' => 'top_image',
															'label' => __('Slide With Top Image', SH_NAME),
														),
														array(
															'value' => 'bottom_image',
															'label' => __('Slide with Bottom Image', SH_NAME),
														),
													),
									),
									array(
										'type' => 'upload',
										'label' => __('Image', SH_NAME),
										'name'  => 'slide_main_image',
									),
									array(
										'type' => 'textbox',
										'name' => 'slide_text',
										'label' => __('Slide Text', SH_NAME),
										'default' => '',									),
									array(
										'type' => 'textbox',
										'name' => 'slide_description',
										'label' => __('Slide Description', SH_NAME),
										'default' => '',	
									),
									array(
										'type' => 'textbox',
										'name' => 'slide_button_text',
										'label' => __('Button Text', SH_NAME),
										'default' => '',
									),
									array(
										'type' => 'textbox',
										'name' => 'slide_button_link',
										'label' => __('Button link', SH_NAME),
										'default' => '',
									),
									array(
										'type' => 'upload',
										'label' => __('Background Image', SH_NAME),
										'name'  => 'slide_background_image',
										'description' => __('show Only if you select Parallax-Slider in ThemeOptions', SH_NAME),
									),
								 ),
			  );
/**
 * EOF
 */
 
 
 return $options;