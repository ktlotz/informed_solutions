<?php
return array(
    'title' => __( 'PaperPlane Theme Options', SH_NAME ),
    'logo' => get_template_directory_uri() . '/images/PaperPlane_theme_logo.png',
    'menus' => array(

        // General Settings
         array(
            'title' => __( 'General Settings', SH_NAME ),
            'name' => 'general_settings',
            'icon' => 'font-awesome:icon-foursquare',
            'menus' => array(
                 
				 array(
                    'title' => __( 'Color Scheme', SH_NAME ),
                    'name' => 'general_settings',
                    'icon' => 'font-awesome:icon-magic',
                    'controls' => array(
						array(
                            'type' => 'section',
                            'repeating' => false,
                            'sortable' => true,
                            'title' => __( 'Color Scheme', SH_NAME ),
                            'name' => 'color_schemes',
                            'description' => __( 'This section is used for theme color settings', SH_NAME ),
                            'fields' => array(
                                
                                array(
                                    'type' => 'radioimage',
                                    'name' => 'custom_color_scheme',
                                    'label' => __( 'Color Scheme', SH_NAME ),
                                    'description' => __( 'Choose the Custom color scheme for the theme.', SH_NAME ),
									'items' => array(
													array(
														'value' => 'custom_color_scheme_black',
														'label' => __('Black', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-1.png' ,
													),
													array(
														'value' => 'style-2',
														'label' => __('Orange', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-2.png' ,
													),
													array(
														'value' => 'style-3',
														'label' => __('Golden-Rod', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-3.png' ,
													),
													array(
														'value' => 'style-4',
														'label' => __('Lawn-Green', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-4.png' ,
													),
													array(
														'value' => 'style-5',
														'label' => __('Green', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-5.png' ,
													),
													array(
														'value' => 'style-6',
														'label' => __('Turquoise', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-6.png' ,
													),
													array(
														'value' => 'style-7',
														'label' => __('Blue', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-7.png' ,
													),
													array(
														'value' => 'style-8',
														'label' => __('Violet', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-8.png' ,
													),
													array(
														'value' => 'style-9',
														'label' => __('Magenta', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-9.png' ,
													),
													array(
														'value' => 'style-10',
														'label' => __('Pink', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-10.png' ,
													),
													array(
														'value' => 'style-11',
														'label' => __('Crimson', SH_NAME),
														'img' => get_template_directory_uri().'/images/style-11.png' ,
													),
									), 
                                ),
								array(
                                    'type' => 'radioimage',
                                    'name' => 'custom_theme_style',
                                    'label' => __( 'Theme Style', SH_NAME ),
                                    'description' => __( 'Choose Theme Style.', SH_NAME ),
									'items' => array(
													array(
														'value' => 'style',
														'label' => __('Plain Theme', SH_NAME),
														'img' => get_template_directory_uri().'/images/plain.png' ,
													),
													array(
														'value' => 'angled',
														'label' => __('Angled Theme', SH_NAME),
														'img' => get_template_directory_uri().'/images/angled.png' ,
													),
											   ), 
                                ),
								                                
                            ) 
                        ),
                    			) 
                ),
                /** Preloader Settings */
				array(
					'title' => __( 'Preloader Settings', SH_NAME ),
					'name' => 'sub_preloader_settings',
					'icon' => 'font-awesome:icon-refresh',
					'controls' => array(
						array(
							'type' => 'toggle',
							'name' => 'cs_preloader',
							'label' => __('Preloader ?', SH_NAME),
							'description' => __( 'Turn it ON if you want to Preloader.', SH_NAME ),
						),
						array(
							'type' => 'upload',
							'name' => 'cs_preloader_image',
							'label' => __('Image', SH_NAME),
							'description' => __('Upload Image for Preloader.', SH_NAME),
							'dependency' => array(
												'field' => 'cs_preloader',
												'function' => 'vp_dep_boolean' 
											)
						),
												
					) 
				),
				/** Submenu for header settings */
                array(
                    'title' => __( 'Header Settings', SH_NAME ),
                    'name' => 'header_settings',
                    'icon' => 'font-awesome:icon-credit-card',
                    'controls' => array(
                        array(
                            'type' => 'upload',
                            'name' => 'site_favicon',
                            'label' => __( 'Favicon', SH_NAME ),
                            'description' => __( 'Upload the favicon, should be 16x16', SH_NAME ),
                            'default' => '' 
                        ),
                        array(
                            'type' => 'upload',
                            'name' => 'site_logo',
                            'label' => __( 'Logo', SH_NAME ),
                            'description' => __( 'Upload Image for Logo', SH_NAME ),
                            'default' => '' 
                        ),
						array(
							'type' => 'slider',
							'name' => 'logo_width',
							'label' => __( 'Logo Width', SH_NAME ),
							'description' => __( 'choose the logo width', SH_NAME ),
							'default' => '69',
							'min' => 20,
							'max' => 400 ,
						),
						array(
							'type' => 'slider',
							'name' => 'logo_height',
							'label' => __( 'Logo Height', SH_NAME ),
							'description' => __( 'choose the logo height', SH_NAME ),
							'default' => '35',
							'min' => 20,
							'max' => 400 ,
						),
                    ) 
                    
                ),
                
                /** Submenu for footer area */
                array(
                    'title' => __( 'Footer Settings', SH_NAME ),
                    'name' => 'sub_footer_settings',
                    'icon' => 'font-awesome:icon-hdd',
                    'controls' => array(
									array(
										'type' => 'section',
										'repeating' => false,
										'sortable'  => false,
										'label' => __('Map Section', SH_NAME),
										'name' => 'footer_map_section',
										'description' => __('This section is used for Map Section', SH_NAME),
										'fields' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_map',
													'label' => __('Show Map', SH_NAME),
													'description' => __( 'Show Map before footer.', SH_NAME ),
												),
												array(
													'type' => 'textbox',
													'name' => 'map_latitude',
													'label' => __('Map Latitude', SH_NAME),
													'description' => __('Enter Latitude For Map.', SH_NAME),
													'default' => __('', SH_NAME),
													'dependency' => array(
																		'field' => 'show_map',
																		'function' => 'vp_dep_boolean' 
																	),
												),
												array(
													'type' => 'textbox',
													'name' => 'map_longitude',
													'label' => __( 'Map Longitude', SH_NAME ),
													'description' => __( 'Enter Longitude For Map', SH_NAME ),
													'default' => __('', SH_NAME),
													'dependency' => array(
																		'field' => 'show_map',
																		'function' => 'vp_dep_boolean' 
																	),
												), 
													),
									),
									array(
										 'type' => 'textarea',
										'name' => 'copyright_text',
										'label' => __( 'Copyright Text', SH_NAME ),
										'description' => __( 'Enter the Copyright Text', SH_NAME ),
										'default' => '' 
									),
									array(
										'type' => 'builder',
										'repeating' => true,
										'sortable' => true,
										'label' => __( 'Social Icons', SH_NAME ),
										'name' => 'social_icons',
										'description' => __( 'This section is used to add Social Icons.', SH_NAME ),
										'fields' => array(
														array(
															'type' => 'select',
															'name' => 'cs_social_media_icon',
															'label' => __('Social Media Icon', SH_NAME),
															'description' => __('Choose Social Media Icon.', SH_NAME),
															'items' => array(
																		'data' => array(
																					array(
																						'source' => 'function',
																						'value' => 'vp_get_social_medias',
																					),
																				  ),
																		),
														),
														array(
															'type' => 'textbox',
															'name' => 'site_contact_icon_title',
															'label' => __( 'Title', SH_NAME ),
															'description' => __( 'Enter Title for social media.', SH_NAME ),
														),
														array(
															'type' => 'textbox',
															'name' => 'site_contact_icon_link',
															'label' => __( 'Link', SH_NAME ),
															'description' => __( 'Enter Link for social media.', SH_NAME ),
															'default' => '#' 
														),
												   ), 
									),
                    ) 
                ), //End of submenu
				
				// SEO General settings Settings
				array(
					 'title' => __( 'SEO Settings', SH_NAME ),
					'name' => 'seo_settings',
					'icon' => 'font-awesome:icon-key',
					
					'controls' => array(
						
						array(
							 'type' => 'toggle',
							'name' => '_enable_seo',
							'label' => __( 'Enable SEO', SH_NAME ),
							'description' => __( 'Enable or disable seo settings', SH_NAME ),
							'default' => 1 
						),
						array( 
								'type' => 'section',
								'title' => __( 'Homepage Settings', SH_NAME ),
								'name' => '_seo_homepage_settings',
								'description' => __( 'homepage meta title, meta description and meta keywords', SH_NAME ),
								'fields' => array(
										array(
											 'type' => 'textbox',
											'name' => '_seo_home_meta_title',
											'label' => __( 'Meta Title', SH_NAME ),
											'description' => __( 'Enter the Title you want to show on home page', SH_NAME ),
											'default' => ''
										),
										array(
											'type' => 'textarea',
											'name' => '_seo_home_meta_description',
											'label' => __( 'Meta Description', SH_NAME ),
											'description' => __( 'Enter the meta description for homepage', SH_NAME ),
											'default' => ''
										),
										array(
											'type' => 'textarea',
											'name' => '_seo_home_meta_keywords',
											'label' => __( 'Meta Keywords', SH_NAME ),
											'description' => __( 'Enter the comma separated keywords for homepage', SH_NAME ),
											'default' => ''
										),
								),
						 ), /** End of homepage seo settings */
						 
						 array( 
								'type' => 'section',
								'title' => __( 'Archive Pages Settings', SH_NAME ),
								'name' => '_seo_archive_settings',
								'description' => __( 'archive pages meta title, meta description and meta keywords', SH_NAME ),
								'fields' => array(
										array(
											 'type' => 'textbox',
											'name' => '_seo_archive_meta_title',
											'label' => __( 'Meta Title', SH_NAME ),
											'description' => __( 'Enter the Title you want to show on home page', SH_NAME ),
											'default' => ''
										),
										array(
											'type' => 'textarea',
											'name' => '_seo_archive_meta_description',
											'label' => __( 'Meta Description', SH_NAME ),
											'description' => __( 'Enter the meta description for homepage', SH_NAME ),
											'default' => ''
										),
										array(
											'type' => 'textarea',
											'name' => '_seo_archive_meta_keywords',
											'label' => __( 'Meta Keywords', SH_NAME ),
											'description' => __( 'Enter the comma separated keywords for homepage', SH_NAME ),
											'default' => ''
										),
								),
						 ),/** End of archive page settings */
						
					), /** End of controls */
						
				), /** End of seo page settings */
            ) 
        ),
		
		// Slider Settings
		array(
			'title' => __( 'Slider Section', SH_NAME ),
			'name' => 'slider_settings',
			'icon' => 'font-awesome:icon-download',
			'controls' => array(
						
						array(
							'type' => 'toggle',
							'name' => 'cs_slider',
							'label' => __('Show Slider ?', SH_NAME),
							'description' => __( 'Turn it ON if you want to Preloader.', SH_NAME ),
						),
						array(
							'type' => 'textbox',
							'name' => 'slider_menu_name',
							'label' => __('Slider Menu Name', SH_NAME),
							'description' => __('Enter Menu Name for Slider', SH_NAME),
							'dependency' => array(
												'field' => 'cs_slider',
												'function' => 'vp_dep_boolean' 
											)						
						),

						 array(
							'type' => 'radioimage',
							'name' => 'theme_layouts',
							'label' => __( 'Layouts', SH_NAME ),
							'description' => __( 'Choose Layout.', SH_NAME ),														                            'default' => 'parallax-image',
							'items' => array(
								 array(
									'value' => 'parallax-image',
									'label' => __('Parallax-Image' , SH_NAME),
									'img' => get_template_directory_uri().'/images/image.png' , 
									
								),
								array(
									'value' => 'parallax-slider',
									'label' => __('Parallax-Slider' , SH_NAME) ,
									'img' => get_template_directory_uri().'/images/slider.png' ,
								) ,
								array(
									'value' => 'video-background',
									'label' => __('Video-Background' , SH_NAME) ,
									'img' => get_template_directory_uri().'/images/video.png' ,
								),
							) 
						),
						array(
							'type' => 'textbox',
							'name' => 'slides_no',
							'label' => __('Number', SH_NAME),
							'description' => __('Enter Number of Slides to Show', SH_NAME),							
						),
						// Parallax-Image Dependent Fileds
						array(
							'type' => 'upload',
							'name' => 'parallax_image_bg',
							'label' => __( 'Parallax Image', SH_NAME ),
							'description' => __( 'Upload Parallax Image to show in slides background.', SH_NAME ),
							'dependency' => array(
											'field' => 'theme_layouts',
											'function' => 'vp_dep_is_image_parallax' 
										)
						),
						// Video-Background Dependent Fileds
						array(
							'type' => 'textbox',
							'name' => 'video_background_link',
							'label' => __('Video Link', SH_NAME),
							'description' => __('Enter Youtube Link Only.', SH_NAME),
							'dependency' => array(
											'field' => 'theme_layouts',
											'function' => 'vp_dep_is_video_background' 
										)						
						),
			) 
	
		),
		
		//Partners Options
      	array(
			'title' => __('Partners Section', SH_NAME),
			'name' => 'partners',
			'icon' => 'font-awesome:icon-flag',
			'controls' => array(
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Paper-Plane Partner', SH_NAME),
					'name' => 'partners',
					'description' => __('This section is used for images of Partners.', SH_NAME),
					'fields' => array(
						
						array(
							'type' => 'upload',
							'name' => 'image',
							'label' => __('Image', SH_NAME),
							'description' => __('Upload Image of a Partner.', SH_NAME),
							'default' => __('', SH_NAME),
						),
						array(
							'type' => 'textbox',
							'name' => 'image_title',
							'label' => __( 'Image Title', SH_NAME ),
							'description' => __( 'Enter Image Title.', SH_NAME ),
						),
						array(
							'type' => 'textbox',
							'name' => 'image_link',
							'label' => __( 'Image Link', SH_NAME ),
							'description' => __( 'Enter Image Link.', SH_NAME ),
						),
					),
				),
			)
		),
		
		//Portfolio Options
      	array(
			'title' => __('Portfolio Section', SH_NAME),
			'name' => 'portfolio',
			'icon' => 'font-awesome:icon-picture',
			'controls' => array(
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Portfolio', SH_NAME),
					'name' => 'portfolios',
					'description' => __('This section is used for Portfolio.', SH_NAME),
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'portfolio_name',
							'label' => __( 'Name', SH_NAME ),
							'description' => __( 'Enter Name.', SH_NAME ),
						),
						array(
							'type' => 'textbox',
							'name' => 'portfolio_link',
							'label' => __( 'Link', SH_NAME ),
							'description' => __( 'Enter Link.', SH_NAME ),
						),
						array(
							'type' => 'select',
							'name' => 'portfolio_pop_up',
							'label' => __( 'Image Pop-Up', SH_NAME ),
							'description' => __( 'Choose Image or Video for Pop-up.', SH_NAME ),
							'items' => array(
											array(
												'value' => 'image',
												'label' => 'Image' 
											),
											array(
												'value' => 'video',
												'label' => 'Video' 
											),
										),
						),
						array(
							'type' => 'textbox',
							'name' => 'popup_video_link',
							'label' => __( 'Pop Up Video Link', SH_NAME ),
						),
						array(
							'type' => 'upload',
							'name' => 'portfolio_image',
							'label' => __('Image', SH_NAME),
							'description' => __('Upload Image for Portfolio.(Larger Size)', SH_NAME),
						),
					),
				),
			)
		),
		
		//Skills Options
      	array(
			'title' => __('Skills Section', SH_NAME),
			'name' => 'skills',
			'icon' => 'font-awesome:icon-wrench',
			'controls' => array(
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Skill', SH_NAME),
					'name' => 'skill',
					'description' => __('This section is used for Skills.', SH_NAME),
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'skill_name',
							'label' => __( 'Name', SH_NAME ),
							'description' => __( 'Enter Name.', SH_NAME ),
						),
						array(
							'type' => 'textbox',
							'name' => 'skill_description',
							'label' => __( 'Text', SH_NAME ),
							'description' => __( 'Enter Description.', SH_NAME ),
						),
						array(
							'type' => 'slider',
							'name' => 'skill_percentage',
							'label' => __( 'Percentage', SH_NAME ),
							'description' => __( 'choose the skill percentage', SH_NAME ),
							'default' => 50,
							'min' => 1,
							'max' => 100 ,
						),
					),
				),
			)
		),
		
		//Testimonial Options
      	array(
			'title' => __('Testimonial Section', SH_NAME),
			'name' => 'testimonial',
			'icon' => 'font-awesome:icon-quote-left',
			'controls' => array(
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Testimonial', SH_NAME),
					'name' => 'testimonials',
					'description' => __('This section is used for Testimonial.', SH_NAME),
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'testimonial_name',
							'label' => __( 'Name', SH_NAME ),
							'description' => __( 'Enter Name.', SH_NAME ),
						),
						array(
							'type' => 'textarea',
							'name' => 'testimonial_text',
							'label' => __( 'Text', SH_NAME ),
							'description' => __( 'Enter Text.', SH_NAME ),
						),
						array(
							'type' => 'upload',
							'name' => 'testimonial_image',
							'label' => __('Image', SH_NAME),
							'description' => __('Upload Image for Testimonial.', SH_NAME),
						),
					),
				),
			)
		),
        
		//Pricing Table Options
      	array(
			'title' => __('Pricing Tables Section', SH_NAME),
			'name' => 'pricing_tables',
			'icon' => 'font-awesome:icon-wrench',
			'controls' => array(
				array(
					'type' => 'textbox',
					'name' => 'feature_number',
					'label' => __( 'Feature Numbers', SH_NAME ),
					'description' => __( 'How many features you want to add in a pricing table.', SH_NAME ),
					'default' => 6,
				),
				 array(
					'type' => 'notebox',
					'name' => 'nb_1',
					'label' => __('Feature Numbers', SH_NAME),
					'description' => __('Features Lines In shortcode Must be equal or less than Feature Number. if Feature Lines exceed than he exceeded line would not be shown in pricing table.', SH_NAME),
					'status' => 'normal',
				),
			)
		),
		
		// Common Page Settings Settings
         array(
            'title' => __( 'Blog', SH_NAME ),
            'name' => 'blog_page_settings',
            'icon' => 'font-awesome:icon-flag',
            'menus' => array(
						//Blog Page
							array(
								'title' => __('Blog Page', SH_NAME),
								'name' => 'blog_posts',
								'icon' => 'font-awesome:icon-bold',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_blog_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'textbox',
													'name' => 'blog_page_title',
													'label' => __( 'Page Title', SH_NAME ),
													'description' => __( 'Enter Page Title.', SH_NAME ),
												),
												array(
													'type' => 'upload',
													'name' => 'title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'blog_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//Archive Page
							array(
								'title' => __('Archive Page', SH_NAME),
								'name' => 'archive_blog_posts',
								'icon' => 'font-awesome:icon-archive',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_archive_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'upload',
													'name' => 'archive_title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'archive_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//Author Page
							array(
								'title' => __('Author Page', SH_NAME),
								'name' => 'author_blog_posts',
								'icon' => 'font-awesome:icon-adn',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_author_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'upload',
													'name' => 'author_title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'author_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//Category Page
							array(
								'title' => __('Category Page', SH_NAME),
								'name' => 'category_blog_posts',
								'icon' => 'font-awesome:icon-sitemap',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_category_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'upload',
													'name' => 'category_title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'category_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//Tag Page
							array(
								'title' => __('Tag Page', SH_NAME),
								'name' => 'tags_blog_posts',
								'icon' => 'font-awesome:icon-tags',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_tags_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'upload',
													'name' => 'tags_title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'tags_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//Search Page
							array(
								'title' => __('Search Page', SH_NAME),
								'name' => 'search_blog_posts',
								'icon' => 'font-awesome:icon-search',
								'controls' => array(
												array(
													'type' => 'toggle',
													'name' => 'show_search_breadcrum',
													'label' => __( 'Show Breadcrum ?', SH_NAME ) 
												),
												array(
													'type' => 'upload',
													'name' => 'search_title_bg_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'select',
													'name' => 'search_page_sidebar',
													'label' => __( 'Sidebar', SH_NAME ),
													'items' =>	array(
																	'data' => array(
																				array(
																					'source' => 'function',
																					'value' => 'sh_get_sidebars_2' 
																				) 
																			  ) 
																),
												),
											  )
							),
							
						//404 Page
							array(
								'title' => __('404 Page', SH_NAME),
								'name' => 'page_404',
								'icon' => 'font-awesome:icon-warning-sign',
								'controls' => array(
												array(
													'type' => 'textbox',
													'name' => 'page_404_title',
													'label' => __( 'Page Title', SH_NAME ),
													'description' => __( 'Enter Page Title.', SH_NAME ),
												),
												array(
													'type' => 'upload',
													'name' => 'title_bg_404_image',
													'label' => __('Image', SH_NAME),
													'description' => __('Upload Title Background Image.', SH_NAME),
												),
												array(
													'type' => 'textbox',
													'name' => 'page_404_heading',
													'label' => __( 'Page Heading', SH_NAME ),
													'description' => __( 'Enter Page Heading.', SH_NAME ),
												),
												array(
													'type' => 'textbox',
													'name' => 'page_404_subheading',
													'label' => __( 'Page Sub-Heading', SH_NAME ),
													'description' => __( 'Enter Page Sub-Heading.', SH_NAME ),
												),
											  )
							),
            ) 
        ),
		
		

		// One Page Settings Section
		array(
			'title' => __('One Page Settings', SH_NAME),
			'name' => 'onepage_settings',
			'icon' => 'font-awesome:icon-pinterest',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'show_one_page_settings',
					'label' => __( 'Show One Page Settings', SH_NAME ) 
				),
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Section', SH_NAME),
					'name' => 'one_page_section',
					'description' => __('This section is used to add a section to your Home Page', SH_NAME),
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'menu_name',
							'label' => __('Menu Name', SH_NAME),
							'description' => __('Menu Name', SH_NAME),
						),
						array(
							'type' => 'select',
							'name' => 'section_page',
							'label' => __( 'Select Page', SH_NAME ),
							'description' => __( 'choose Page', SH_NAME ),
							'items' => array(
										'data' => array(
													array(
														'source' => 'function',
														'value' => 'sh_vp_pages_list',
													),
												  ),
									   ), 
						),
					),
				),
				array(
					'type' => 'builder',
					'repeating' => true,
					'sortable'  => true,
					'label' => __('Links', SH_NAME),
					'name' => 'one_page_links',
					'description' => __('Add Links to Menus', SH_NAME),
					'fields' => array(
									array(
										'type' => 'textbox',
										'name' => 'menu_links_name',
										'label' => __( 'Menu Name', SH_NAME ),
										'description' => __( '', SH_NAME ),
										'default' => '' ,
									),
									array(
										'type' => 'textbox',
										'name' => 'menu_link',
										'label' => __( 'Menu Link', SH_NAME ),
										'description' => __( '', SH_NAME ),
										'default' => '#' 
									),
								),
				),
				
			),
		),
		
		// Coming-Soon Page Section
		array(
			'title' => __('Coming-Soon Page Settings', SH_NAME),
			'name' => 'coming_soon_settings',
			'icon' => 'font-awesome:icon-gittip',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'coming_soon_template',
					'label' => __('Show Coming Soon Page ?', SH_NAME),
					'description' => __( 'Turn it ON if your theme is not ready.', SH_NAME ),
				),
				array(
					'type' => 'section',
					'repeating' => false,
					'sortable' => true,
					'title' => __( 'Slider', SH_NAME ),
					'name' => 'coming_slider',
					'description' => __( 'This section is used for Long Alert Box Settings', SH_NAME ),
					'fields' => array(
									array(
										'type' => 'toggle',
										'name' => 'coming_soon_slider',
										'label' => __('Show Slider ?', SH_NAME),
										'description' => __( 'Turn it ON if you want to Show Slider.', SH_NAME ),
									),
									array(
										'type' => 'notebox',
										'name' => 'coming_slider_note',
										'label' => __('Slides Settings', SH_NAME),
										'description' => __('All Slider Settings are controlled from Appearance -> Theme Options -> Slider Settings.', SH_NAME),
										'status' => 'normal',
										'dependency' => array(
															'field' => 'coming_soon_slider',
															'function' => 'vp_dep_boolean' 
														)
									),
                            	) 
               ),
				array(
					'type' => 'section',
					'repeating' => false,
					'sortable' => true,
					'title' => __( 'Alert Box', SH_NAME ),
					'name' => 'coming_alert',
					'description' => __( 'This section is used for Long Alert Box Settings', SH_NAME ),
					'fields' => array(
									array(
										'type' => 'toggle',
										'name' => 'coming_soon_alert',
										'label' => __('Show Alert Box Section?', SH_NAME),
										'description' => __( 'Turn it ON if you want to show Alert Box.', SH_NAME ),
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_alert_tagline',
										'label' => __('Tagline', SH_NAME),
										'description' => __('Enter Tagline for Alert Box', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_alert',
															'function' => 'vp_dep_boolean' 
														)						
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_alert_button_text',
										'label' => __('Button Text', SH_NAME),
										'description' => __('Enter Button Text for Alert Box', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_alert',
															'function' => 'vp_dep_boolean' 
														)						
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_alert_button_link',
										'label' => __('Button Link', SH_NAME),
										'description' => __('Enter Button Link for Alert Box', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_alert',
															'function' => 'vp_dep_boolean' 
														)						
									),
                            	) 
               ),
			   	array(
					'type' => 'section',
					'repeating' => false,
					'sortable' => true,
					'title' => __( 'Time to Open', SH_NAME ),
					'name' => 'coming_time_open',
					'description' => __( 'This section is used for Time to Open Section Settings', SH_NAME ),
					'fields' => array(
									array(
										'type' => 'toggle',
										'name' => 'coming_soon_time',
										'label' => __('Show Time to open Section ?', SH_NAME),
										'description' => __( 'Turn it ON if you want to show Time to Open Section.', SH_NAME ),
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_time_title',
										'label' => __('Title', SH_NAME),
										'description' => __('Enter Title for Time to Open Section', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_time',
															'function' => 'vp_dep_boolean' 
														)						
									),
									 array(
										'type' => 'date',
										'name' => 'coming_time_date',
										'label' => __('Date', SH_NAME),
										'description' => __('How much time remains to open Theme' , SH_NAME ),
										'format' => 'dd-mm-yy',
										'dependency' => array(
															'field' => 'coming_soon_time',
															'function' => 'vp_dep_boolean' 
														)	
									),
                            	) 
               ),
			   	array(
					'type' => 'section',
					'repeating' => false,
					'sortable' => true,
					'title' => __( 'Newsletter Section', SH_NAME ),
					'name' => 'coming_newsletter',
					'description' => __( 'This section is used for Newsletter Section Settings', SH_NAME ),
					'fields' => array(
									array(
										'type' => 'toggle',
										'name' => 'coming_soon_newsletter',
										'label' => __('Show Newsletter Section ?', SH_NAME),
										'description' => __( 'Turn it ON if you want to show Newsletter Section.', SH_NAME ),
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_newsletter_title',
										'label' => __('Title', SH_NAME),
										'description' => __('Enter Title for Newsletter Section', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_newsletter',
															'function' => 'vp_dep_boolean' 
														)						
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_newsletter_button',
										'label' => __('Button Text', SH_NAME),
										'description' => __('Enter Text for Button', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_newsletter',
															'function' => 'vp_dep_boolean' 
														)						
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_newsletter_feed_id',
										'label' => __('Feed Burner ID', SH_NAME),
										'description' => __('Enter ID for Newsletter Section', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_newsletter',
															'function' => 'vp_dep_boolean' 
														)						
									),
									array(
										'type' => 'upload',
										'name' => 'coming_newsletter_bg_image',
										'label' => __('Background Image', SH_NAME),
										'description' => __('Upload Image for Section Background.', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_newsletter',
															'function' => 'vp_dep_boolean' 
														)
									),
                            	) 
               ),
			   	array(
					'type' => 'section',
					'repeating' => false,
					'sortable' => true,
					'title' => __( 'Contact Section', SH_NAME ),
					'name' => 'coming_contact',
					'description' => __( 'This section is used for Contact Section Settings', SH_NAME ),
					'fields' => array(
									array(
										'type' => 'toggle',
										'name' => 'coming_soon_contact',
										'label' => __('Show Contact Section ?', SH_NAME),
										'description' => __( 'Turn it ON if you want to show Contact Section.', SH_NAME ),
									),
									array(
										'type' => 'textbox',
										'name' => 'coming_contact_title',
										'label' => __('Title', SH_NAME),
										'description' => __('Enter Title for Contact Section', SH_NAME),
										'dependency' => array(
															'field' => 'coming_soon_contact',
															'function' => 'vp_dep_boolean' 
														)						
									),
                            	) 
               ),
			   	array(
					'type' => 'notebox',
					'name' => 'coming_setting_note',
					'label' => __('Slides Settings', SH_NAME),
					'description' => __('All Header Settings are controlled from Appearance -> Theme Options -> General Settings -> Header Settings. <br> & <br>All Footer Settings are controlled from Appearance -> Theme Options -> General Settings -> Footer Settings.', SH_NAME),
					'status' => 'normal',
				),
			),
		),
    ) 
);

/**
 *EOF
 */