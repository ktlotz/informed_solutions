<?php 
	get_header();
	sh_post_views();
	$page_meta = get_post_meta(get_the_ID(), 'page_meta', true);
	$page_id = (get_the_title())? 'home-blog-section':'home-blog-section-without-title';
?>
<div style="background-image:url(<?php echo esc_url(sh_set($page_meta , 'image'));?>)" id="<?php echo esc_attr($page_id); ?>">
<div id="home-section-overlayer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if(get_the_title()):?>
				<div class="blog-title">
					<?php the_title(); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php if(sh_set($page_meta , 'show_breadcrum')): ?>
<div id="blog-breadcrumb-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php echo get_the_breadcrumb(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<div id="blog-content-section">
	<?php 
		while(have_posts()): the_post(); 
	 ?>
		<div <?php post_class('row'); ?>>
			<div class="col-md-12">
				<div class="post-container">
					<div class="post-content">
						<?php the_content(); ?>
					</div>
					<div class="container">
					<div class="post-share">

						<span>
							<?php echo getPostLikeLink( $post->ID ); ?> 
						</span>
						<?php if(sh_set($page_meta , 'show_comment')): ?>
						<span>
							<a href="#comment_view" title="<?php esc_attr_e("Comments" , SH_NAME); ?>"  data-scroll>
								<i class="pe-7s-comment"></i> <?php comments_number(); ?>
							</a>
						</span>
						<?php endif; ?>
						<span>
							<i class="pe-7s-look"></i> <?php echo get_post_meta( $id, '_pp_post_views', true); ?>
						</span>
						<span>
						<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=undefined"></script>
						 <a class="addthis_button_compact"> 
						 	<span>
								<i class="pe-7s-share"></i> 
								<?php esc_html_e("Share" , SH_NAME); ?> 
							</span> 
						 </a> 
						</span>
					</div><!-- /post-share -->
					</div>
				</div><!-- /post-container -->
				<div class="container">
				<?php
					if(sh_set($page_meta , 'show_comment')):
						comments_template();
					endif;
					
					wp_link_pages();
				?>
				</div>
			</div>
			
		</div>
	<?php endwhile; ?>

</div>

<script>
jQuery(document).ready(function($) {
	
	 jQuery(this).find('.post-image img').each(function () {
		jQuery(this).addClass('grayscale');
	});
	
	jQuery(this).find('.avatar-image img').each(function () {
        jQuery(this).addClass('grayscale');
    });
});
</script>

<?php get_footer(); ?>