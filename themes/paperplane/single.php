<?php 
	get_header();
	sh_post_views();
	$meta = get_post_meta(get_the_ID() , 'post_meta' , true); 
	$post_icon = sh_set($meta , 'sh_post_social');
?>
<div style="background-image: url(<?php echo esc_url(sh_set($meta , 'image')); ?>);" id="home-blog-section">
<div id="home-section-overlayer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="blog-title">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="blog-breadcrumb-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<?php
				if(sh_set($meta , 'breadcrum')):
					echo get_the_breadcrumb();
				endif;
			?>
			</div>
		</div>
	</div>
</div>

<div id="blog-content-section">
	<div class="container">
	<?php 
		while(have_posts()): the_post(); 
		$main_class = (sh_set($meta , 'sidebar')) ? 'col-md-9' : 'col-md-12' ;
	 ?>
		<div <?php post_class('row'); ?>>
			<div class="<?php echo esc_attr($main_class) ; ?>">
				<div class="post-container">
					<?php if ( has_post_thumbnail() ): ?>
						<div class="post-image">
							<?php the_post_thumbnail('845x530', array( 'class' => 'img-responsive' )); ?>
						</div>
					<?php endif; ?>
					<div class="post-title">
						<?php the_title(); ?>
					</div>
					<div class="post-meta">
						<span><?php echo get_the_date('j'); ?></span>
						<span><?php echo get_the_date('M'); ?></span>
					</div>
					<div class="post-content">
						<?php the_content(); ?>
					</div>
					<?php 
						$posttags = get_the_tags();
						$categories_list = get_the_category_list( __( ', ', get_the_ID() ) );
						if($posttags || $categories_list):
					?>
					<div class="cat_tag post-share">
						<span>
							<i class="fa fa-user padg_left" title="<?php esc_attr_e("Author" , SH_NAME) ; ?>"></i>
							<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a>
							<?php if($posttags): ?>
							  &nbsp;
							  <i class="fa fa-tag padg_left" title="<?php esc_attr_e("Tags" , SH_NAME) ; ?>"></i>
							<?php 
								the_tags(' ', ' , ' , ' '); 
								endif;
							?>
							&nbsp;&nbsp;
							<i class="fa fa-folder-open padg_left" title="<?php esc_attr_e("Categories" , SH_NAME) ; ?>"></i>
							  <?php the_category( ' , ');	?>
						</span>
					</div>
					<?php endif; ?>
					<div class="post-share">

						<span>
							<?php echo getPostLikeLink( get_the_ID() ); ?> 
						</span>
						
						<span>
							<a href="#comment_view" title="<?php esc_html_e("Comments" , SH_NAME); ?>"  data-scroll>
								<i class="pe-7s-comment"></i> <?php comments_number(); ?>
							</a>
						</span>
						<span>
							<i class="pe-7s-look"></i> <?php echo get_post_meta( $id, '_pp_post_views', true); ?>
						</span>
						<span>
						<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=undefined"></script>
						 <a class="addthis_button_compact"> <span><i class="pe-7s-share"></i> <?php esc_html_e("Share" , SH_NAME); ?> </span> </a> 
						</span>
					</div><!-- /post-share -->
		
				</div><!-- /post-container -->
		
				<?php comments_template(); ?>
				<?php wp_link_pages(); ?>
		
		
			</div>
		
			<?php if(sh_set($meta , 'sidebar')): ?>
			<div class="col-md-3">
				<?php dynamic_sidebar(sh_set($meta , 'sidebar')); ?>
			</div>
			<?php endif; ?>
			
		</div>
	<?php endwhile; ?>
	</div>

</div>

<script>
jQuery(document).ready(function($) {
	
	 jQuery(this).find('.post-image img').each(function () {
		jQuery(this).addClass('grayscale');
	});
	
	jQuery(this).find('.avatar-image img').each(function () {
        jQuery(this).addClass('grayscale');
    });
});
</script>

<?php get_footer(); ?>