<?php 
	/* Template Name: Visual Composer */
	get_header(); 
	$theme_options = _WSH()->option();
	$one_page_check = sh_set($theme_options , 'show_one_page_settings');
	$one_page_setting = sh_set(sh_set($theme_options , 'one_page_section') , 'one_page_section');
	$page_links = sh_set(sh_set($theme_options , 'one_page_links') , 'one_page_links');
	
	if($one_page_check):
	
	  foreach($one_page_setting as $section): 
 		if(sh_set($section , 'tocopy')) break;
?>
		
<section id="sec_<?php echo esc_html(sh_set($section , 'section_page')); ?>" style="padding:0px !important; margin:0px !important;">

<h2 style="display:none">empty</h2>
	
	<?php 
	
		$page_data = get_page(sh_set($section , 'section_page'));
	
		echo do_shortcode(sh_set($page_data , 'post_content')) ;
	
	?>
</section>

<?php
	  endforeach; 
	  
	endif;
	
	if(!$one_page_check):
	
		if (have_posts()): 
			while (have_posts()): 
				the_post();		
				the_content();
	 		endwhile; 
		endif;
		 
	endif;
	
get_footer(); 
?>